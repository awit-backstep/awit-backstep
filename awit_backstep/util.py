"""Utility functions for AWIT Backstep."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import os
from typing import IO, Any, Generator, Iterator, Optional, Union

__all__ = ["chunks", "human_bytes", "get_file_size", "json_loads_stream"]


def chunks(file_handle: Optional[IO[Any]], delim: str = "\n", encoding: str = "UTF-8") -> Generator[str, None, str]:
    r"""
    Chunk the return of a file read based on delim character(s).

    Parameters
    ----------
    file_handle : :class:`Optional` [ :class:`IO` [ :class:`Any` ] ]
        File handle to read from.

    delim : :class:`Optional` [ class:`str` ]
        Delimiter to split chunks on, defaults to "\n".

    encoding : :class:`str`
        File encoding, defaults to "UTF-8".

    Yeilds
    ------
    :class:`str` :
        Chunked string.

    """

    if not file_handle:
        return ""

    # Initialize buffer
    buf = bytearray()

    # Loop while we're reading
    while True:  # pylint: disable=while-used
        read_char = file_handle.read(1)
        # Check for no character received
        if read_char == b"":
            return buf.decode(encoding)
        # Check if the character is in the delimeter list
        if read_char in bytes(delim, encoding="UTF-8"):
            buf_str = buf.decode(encoding)
            # If the buffer is not empty, return the decoded version
            if buf_str:
                yield buf_str
            # Re-initialize the byte array
            buf = bytearray()
        # Else, add the character to the buffer
        else:
            buf += read_char


def human_bytes(num: Union[int, float]) -> str:
    """
    Convert a byte counter into something more humanly readable.

    :param num: Byte counter to convert
    :type num: int
    :returns: human readable string representation
    :rtype: str
    """
    step_unit = 1024.0

    for quantifier in ("B", "KiB", "MiB", "GiB", "TiB", "PiB"):
        if num < step_unit:
            return f"{num:3.1f} {quantifier}"
        num /= step_unit

    return f"{num:.1f} EiB"


def get_file_size(filename: str) -> int:
    """Return filesize by seeking to the end."""
    handle = os.open(filename, os.O_RDONLY)
    try:
        return os.lseek(handle, 0, os.SEEK_END)
    finally:
        os.close(handle)


def json_loads_stream(json_str: str) -> Iterator[Any]:
    """Streat concatenated JSON objects."""
    # Create a safe decoder
    decoder = json.JSONDecoder(object_hook=None, object_pairs_hook=None)
    # Set string pos to 0
    pos = 0
    # Strip whitespace from string
    json_str_stripped = json_str.strip()
    # Loop while we still have data
    while pos < len(json_str):  # pylint: disable=while-used
        json_str = json_str_stripped[pos:]
        if not json_str:
            break  # Blank line case
        json_obj, pos = decoder.raw_decode(json_str)
        yield json_obj
