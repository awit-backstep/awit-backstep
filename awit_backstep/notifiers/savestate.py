"""AWIT Backstep savestate notifier."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import json
import os
from typing import Any, Dict

from ..stats import BackstepStats
from . import NotifierError, NotifierList, NotifierPluginBase

__all__ = ["EmailPlugin"]

__VERSION__ = "0.1.1"

STATE_DIR = "/var/lib/awit-backstep"


class EmailPlugin(NotifierPluginBase):
    """Save state notifier plugin."""

    _name = "savestate"

    def register_parser(self, notifier_parser: argparse.ArgumentParser) -> None:
        """
        Register command line parser options.

        :param notifier_parser: Notifier parser subparser
        :type notifier_parser: argparse.ArgumentParser
        """
        notifier_parser.add_argument("--notifier-savestate", action="store_true", help="Use the savestate notifier")

    def use_if_enabled(self, command_args: Dict[str, Any], notifiers: NotifierList) -> None:
        """
        Add this notifier to the list if its enabled.

        Parameters
        ----------
        command_args : :class:`Dict` [ :class:`str` , :class:`Any` ]
            Parsed commandline arguments.

        notifiers : :class:`NotifierList`
            Notifier list to add notifier to if it is enabled.

        """

        if ("notifier_savestate" in command_args) and command_args["notifier_savestate"]:
            # Add ourselves as a notifier
            notifiers.add(self)

    def log(self, level: int, message: str) -> None:
        """
        Log a message. This needs to be overridden.

        :param level: Log level
        :type level: int
        :param message: Log message
        :type message: str
        """
        # We don't log anything, we just output stats to a file

    def send_stats(self, topic: str, stats: BackstepStats) -> None:
        """
        Send the backup stats.

        Parameters
        ----------
        topic : :class:`str`
            Stats topic.

        stats : :class:`BackupStats`
            Backup stats to send.

        Raises
        ------
        NotifierError
            Raised if the state file cannot be created.

        """

        # Make sure we have a name attribute
        if "name" not in stats:
            raise NotifierError(f'BackstepStats does not contain "name" attribute: {stats}')

        if not os.path.isdir(STATE_DIR):
            # Try to create the directory we need
            try:
                os.mkdir(STATE_DIR, mode=0o750)
            except OSError as err:
                raise NotifierError(f'Failed to create directory "{STATE_DIR}": {err}') from None

        state_filename = f'{STATE_DIR}/{stats["name"]}.state'
        # Grab our JSON represenatation of our stats
        stats_json = json.dumps(stats)

        # Try write the data out
        try:
            with open(state_filename, "w", 0o640, encoding="UTF-8") as state_file:
                state_file.write(stats_json)
        except OSError as err:
            raise NotifierError(f'Error writing to file "{state_filename}": {err}') from None
