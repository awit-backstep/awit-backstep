"""AWIT Backstep email notifier."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
import platform
import pwd
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Any, Dict, List

from ..stats import BackstepStats
from . import NotifierList, NotifierPluginBase

__all__ = ["EmailPlugin"]

__VERSION__ = "0.1.1"


class EmailPlugin(NotifierPluginBase):
    """Email notifier plugin."""

    _name = "email"
    _logs: List[str]
    _email_from: str
    _email_to: str

    def __init__(self) -> None:
        """Initialize object."""

        # Call parent to initialize
        super().__init__()

        self._logs = []
        # Grab username, hostname and work out our default email FROM and TO
        username = pwd.getpwuid(os.getuid()).pw_name
        hostname = platform.node()
        self._email_from = f"{username}@{hostname}"
        self._email_to = self._email_from

    def register_parser(self, notifier_parser: argparse.ArgumentParser) -> None:
        """
        Register command line parser options.

        :param notifier_parser: Notifier parser subparser
        :type notifier_parser: argparse.ArgumentParser
        """

        notifier_parser.add_argument("--notifier-email", action="store_true", help="Use the email notifier")
        notifier_parser.add_argument("--notifier-email-from", type=str, help=f'Email from address, defaults to "{self.email_from}"')
        notifier_parser.add_argument("--notifier-email-to", type=str, help=f'Email recipients, defaults to "{self.email_to}"')

    def use_if_enabled(self, command_args: Dict[str, Any], notifiers: NotifierList) -> None:
        """
        Add this notifier to the list if its enabled.

        Parameters
        ----------
        command_args : :class:`Dict` [ :class:`str` , :class:`Any` ]
            Parsed commandline arguments.

        notifiers : :class:`NotifierList`
            Notifier list to add notifier to if it is enabled.

        """

        if ("notifier_email" in command_args) and command_args["notifier_email"]:
            # Check what to use for the TO and FROM
            if ("notifier_email_from" in command_args) and command_args["notifier_email_from"]:
                self._email_from = command_args["notifier_email_from"]
            if ("notifier_email_to" in command_args) and command_args["notifier_email_to"]:
                self._email_to = command_args["notifier_email_to"]
            # Add ourselves as a notifier
            notifiers.add(self)

    def flush(self) -> None:
        """Flush any logs being kept for later sending."""
        self._logs = []

    def log(self, level: int, message: str) -> None:
        """
        Log a message. This needs to be overridden.

        :param level: Log level
        :type level: int
        :param message: Log message
        :type message: str
        """
        self._logs.append(self._logmsg(level, message))

    def send_stats(self, topic: str, stats: BackstepStats) -> None:
        """
        Send the backup stats.

        :param topic: Stats topic
        :type topic: str
        :param stats: Backup stats
        :type stats: BackstepStats
        """

        # Call super to get the stats output in info()
        super().send_stats(topic, stats)

        # Create the email message
        msg = MIMEMultipart()
        msg["Subject"] = f"[AWIT-BACKSTEP] {topic}"
        msg["From"] = self.email_from
        msg["To"] = self.email_to
        msg.preamble = "This is a multi-part message in MIME format.\n"

        # Create nice fancy HTML output
        backup_data = "\n".join(self._logs)
        backup_data_html = f"<pre>{backup_data}</pre>"
        # Attach message part
        part1 = MIMEText(backup_data_html, "html")
        msg.attach(part1)

        # Send the email via our own SMTP server.
        with smtplib.SMTP("localhost") as smtp_connection:
            smtp_connection.send_message(msg)

        # Flush the logs
        self.flush()

    @property
    def email_from(self) -> str:
        """Email FROM address."""
        return self._email_from

    @property
    def email_to(self) -> str:
        """Email TO address."""
        return self._email_to
