"""AWIT Backstep console notifier."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
from typing import Any, Dict

from . import NotifierList, NotifierPluginBase

__all__ = ["ConsolePlugin"]

__VERSION__ = "0.1.1"


class ConsolePlugin(NotifierPluginBase):
    """Console notifier plugin."""

    _name = "console"

    def register_parser(self, notifier_parser: argparse.ArgumentParser) -> None:
        """
        Register command line parser options.

        :param notifier_parser: Notifier parser subparser
        :type notifier_parser: argparse.ArgumentParser
        """
        notifier_parser.add_argument("--notifier-console-disable", action="store_true", help="Disable the console notifier")

    def use_if_enabled(self, command_args: Dict[str, Any], notifiers: NotifierList) -> None:
        """
        Add this notifier to the list if its enabled.

        Parameters
        ----------
        command_args : :class:`Dict` [ :class:`str` , :class:`Any` ]
            Parsed commandline arguments.

        notifiers : :class:`NotifierList`
            Notifier list to add notifier to if it is enabled.

        """

        # Check if we've been disabled
        if ("notifier_console_disable" not in command_args) or not command_args["notifier_console_disable"]:
            notifiers.add(self)

    def log(self, level: int, message: str) -> None:
        """
        Log a message.

        :param level: Log level
        :type level: int
        :param message: Log message
        :type message: str
        """
        print(self._logmsg(level, message))
