"""AWIT Backstep destinations."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import datetime
from typing import Any, Dict, List, Union

from ..plugin import Plugin
from ..stats import BackstepStats
from ..util import human_bytes

__all__ = [
    "LOG_DEBUG",
    "LOG_INFO",
    "LOG_NOTICE",
    "LOG_WARNING",
    "LOG_ERROR",
    "NotifierBase",
    "NotifierError",
    "NotifierList",
    "NotifierPluginBase",
]

# Log level constants
LOG_DEBUG = 5
LOG_INFO = 4
LOG_NOTICE = 3
LOG_WARNING = 2
LOG_ERROR = 1


def _verbosity_to_str(level: int) -> str:  # noqa: CFQ004
    """
    Get the verbosity level from integer.

    :param level: Integer version of the verbosity
    :type level: int
    :returns: string version of the verbosity
    :rtype: str
    """

    # Work out level
    if level == LOG_DEBUG:
        return "DEBUG"
    if level == LOG_INFO:
        return "INFO"
    if level == LOG_NOTICE:
        return "NOTICE"
    if level == LOG_WARNING:
        return "WARNING"
    if level == LOG_ERROR:
        return "ERROR"

    raise RuntimeError(f'Invalid integer log level specified "{level}"')


def _verbosity_to_int(level: str) -> int:  # noqa: CFQ004
    """
    Get the verbosity level from string.

    :param level: String version of the verbosity
    :type level: str
    :returns: integer version of the verbosity
    :rtype: int
    """

    # Work out level
    if level == "DEBUG":
        return LOG_DEBUG
    if level == "INFO":
        return LOG_INFO
    if level == "NOTICE":
        return LOG_NOTICE
    if level == "WARNING":
        return LOG_WARNING
    if level == "ERROR":
        return LOG_ERROR

    raise RuntimeError("Invalid log level specified")


class NotifierError(RuntimeError):
    """Notifier error."""


class NotifierBase:
    """Notifier base."""

    def __init__(self) -> None:
        """Initialize object."""

    def debug(self, message: str) -> None:
        """
        Log an DEBUG message.

        :param message: Log message
        :type message: str
        """
        self.log(LOG_DEBUG, message)

    def info(self, message: str) -> None:
        """
        Log an INFO message.

        :param message: Log message
        :type message: str
        """
        self.log(LOG_INFO, message)

    def notice(self, message: str) -> None:
        """
        Log an NOTICE message.

        :param message: Log message
        :type message: str
        """
        self.log(LOG_NOTICE, message)

    def warning(self, message: str) -> None:
        """
        Log an WARNING message.

        :param message: Log message
        :type message: str
        """
        self.log(LOG_WARNING, message)

    def error(self, message: str) -> None:
        """
        Log an ERROR message.

        :param message: Log message
        :type message: str
        """
        self.log(LOG_ERROR, message)

    def flush(self) -> None:
        """Flush any logs being kept for later sending."""

    def log(self, level: int, message: str) -> None:
        """
        Log amessage. This needs to be overridden.

        :param level: Log level
        :type level: int
        :param message: Log message
        :type message: str
        """

    def _logmsg(self, level: int, message: str) -> str:  # pylint: disable=no-self-use
        """
        Return the message we're going to log, timestamped and pretty.

        :param level: Level of log message, either LOG_DEBUG, LOG_INFO, LOG_NOTICE, LOG_WARNING or LOG_ERROR
        :type level: int
        :param message: Message to log
        :type message: str
        :returns: string to log
        :rtype: str
        """

        level_str = _verbosity_to_str(level).rjust(7)

        return f"{datetime.datetime.now():%Y-%m-%d %H:%M:%S}: {level_str}: {message}"

    def send_stats(self, topic: str, stats: BackstepStats) -> None:
        """
        Send the backup stats.

        :param topic: Stats topic
        :type topic: str
        :param stats: Backup stats
        :type stats: BackstepStats
        """


class NotifierPluginBase(Plugin, NotifierBase):
    """Notifier plugin."""

    _name: str

    def register_notifier(self, **kwargs: Any) -> str:  # pylint: disable=unused-argument
        """Register this notifier."""
        return self.name

    def register_parser(self, notifier_parser: argparse.ArgumentParser) -> None:
        """
        Register command line parser options.

        :param notifier_parser: Notifier parser subparser
        :type notifier_parser: argparse.ArgumentParser
        """

    def use_if_enabled(self, command_args: Dict[str, Any], notifiers: "NotifierList") -> None:
        """
        Add this notifier to the list if its enabled.

        Parameters
        ----------
        command_args : :class:`Dict` [ :class:`str` , :class:`Any` ]
            Parsed commandline arguments.

        notifiers : :class:`NotifierList`
            Notifier list to add notifier to if it is enabled.

        """

    def log(self, level: int, message: str) -> None:
        """
        Log amessage. This needs to be overridden.

        :param level: Log level
        :type level: int
        :param message: Log message
        :type message: str
        """
        raise NotImplementedError

    def send_stats(self, topic: str, stats: BackstepStats) -> None:
        """
        Send the backup stats.

        :param topic: Stats topic
        :type topic: str
        :param stats: Backup stats
        :type stats: BackstepStats
        """

        # If we don't have any stats, just return
        if not stats:
            self.notice("No backup stats available")
            return

        self.info("Backup stats:")
        # Work out displaying the stats in a more easy to understand manner
        for key, val in stats.items():
            # Convert sizes into something human readable
            if "size" in key:
                self.info(f"  {key} = {human_bytes(val)}")
            # Display duration as seconds
            elif "duration" in key:
                self.info(f"  {key} = {val:.0f}s")
            # Display the ratio as a percent
            elif "_ratio" in key:
                self.info(f"  {key} = {val}%")
            # Everything else, display as normal
            else:
                self.info(f"  {key} = {val}")

    @property
    def name(self) -> str:
        """
        Name property.

        :returns: Name of notifier
        :rtype: str
        """

        return self._name


class NotifierList(NotifierBase):
    """Notifier list."""

    _notifiers: List[NotifierPluginBase]
    _topic: str
    _verbosity: int

    def __init__(self, verbosity: Union[str, int] = LOG_NOTICE) -> None:
        """Initialize object."""

        # Initialize parent
        super().__init__()

        self._notifiers = []
        self._topic = ""
        self.set_verbosity(verbosity)

    def set_verbosity(self, verbosity: Union[int, str]) -> None:
        """
        Set verbosity level either with a string or integer.

        :param verbosity: Verbosity level
        :type verbosity: Union[int, str]
        """
        # Set initial verbosity
        if isinstance(verbosity, int):
            self._verbosity = verbosity
        else:
            self._verbosity = _verbosity_to_int(verbosity)

    def add(self, notifier: NotifierPluginBase) -> None:
        """
        Add a notifier.

        :param notifier: Notifier to add
        :type notifier: NotifierPluginBase
        """

        # Only add a notifier if it doesn't already exist
        if notifier.name not in [x.name for x in self._notifiers]:
            self._notifiers.append(notifier)

    def remove_all(self) -> None:
        """Remove all notifiers."""
        self._notifiers = []

    def flush(self) -> None:
        """Flush any logs being kept for later sending."""

        for notifier in self._notifiers:
            notifier.flush()

    def log(self, level: int, message: str) -> None:
        """
        Log a message. This needs to be overridden.

        :param level: Log level
        :type level: int
        :param message: Log message
        :type message: str
        """

        for notifier in self._notifiers:
            if self._should_log(level):
                notifier.log(level, message)

    def send_stats(self, topic: str, stats: BackstepStats) -> None:
        """
        Send backup stats.

        :param topic: Stats topic
        :type topic: str
        :param stats: Backup stats
        :type stats: BackstepStats
        """

        # Loop with notifiers and send the stats off
        for notifier in self._notifiers:
            try:
                notifier.send_stats(topic, stats)
            # If we get an eerror report it as a warning
            except NotifierError as err:
                self.warning(f'Error during send_stats with notifier "{notifier.name}": {err}')

    def _should_log(self, level: int) -> bool:
        """
        Check if we should log the message or not.

        :param level: Log level
        :type level: int
        """
        return level <= self._verbosity

    @property
    def is_default_verbosity(self) -> bool:
        """
        Return if we're default verbosity or not.

        :returns: verbosity level
        :rtype: int
        """
        return self._verbosity == LOG_NOTICE

    @property
    def verbosity(self) -> int:
        """
        Verbosity property.

        :returns: verbosity level
        :rtype: int
        """
        return self._verbosity

    @verbosity.setter
    def verbosity(self, level: int) -> None:
        """
        Set verbosity level from string.

        :param level: String verbosity level.
        :type level: int
        """
        self._verbosity = level
