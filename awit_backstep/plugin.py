"""Plugin handler for AWIT Backstep."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import inspect
import os
import pkgutil
from typing import Any, Dict, List

__all__ = ["Plugin", "PluginCollection", "PluginMethodException", "PluginNotFoundException"]


class PluginMethodException(RuntimeError):
    """Plugin method exception raised when a method is called that does not exist."""


class PluginNotFoundException(RuntimeError):
    """Plugin not found exception raised when a plugin is referenced by name and not found."""


class Plugin:  # pylint: disable=too-few-public-methods
    """Base plugin class, used as the parent for all plugins we define."""

    description: str
    order: int

    def __init__(self) -> None:
        """Plugin __init__ method."""


def _plugin_super_has(obj: Plugin, method_name: str) -> bool:
    """Check if the plugin defines the method."""

    # Grab super class
    super_cls = super(obj.__class__, obj.__class__)
    if getattr(obj.__class__, method_name) != getattr(super_cls, method_name):
        return True
    return False


class PluginCollection:
    """
    Initialize Plugincollection using a plugin base package.

    Apon loading each plugin will be instantiated as an object.

    : param plugin_package: Package name to load plugins from.
    : type plugin_package: str
    """

    # The package name we will be loading plugins from
    _plugin_packages: List[str]
    # List of plugins we've loaded
    _plugins: Dict[str, Plugin]
    # List of paths we've seen during processing
    _seen_paths: List[str]
    # Plugin statuses
    _plugin_status: Dict[str, str]

    def __init__(self, plugin_packages: List[str]):
        """
        Initialize Plugincollection using a plugin base package.

        Classes with a name ending in 'Base' will not be loaded.

        :param plugin_package: Package name to load plugins from.
        :type plugin_package: str
        """

        # Setup object
        self._plugin_packages = plugin_packages
        self._plugins = {}
        self._seen_paths = []
        self._plugin_status = {}

        # Load plugins
        self._load_plugins()

    def call_if_exists(self, method_name: str, **kwargs: Any) -> Any:
        r"""
        Call a plugin method, but do not raise an exception if it does not exist.

        :param method_name: Method name to call.
        :type method_name: str
        :param \**kwargs: kwargs to pass to the method
        """
        return self.call(method_name, skip_not_found=True, **kwargs)

    def call(self, method_name: str, skip_not_found: bool = False, **kwargs: Any) -> Dict[str, Any]:
        """
        Call a plugin method.

        Parameters
        ----------
        method_name : :class:`str`
            Method name to call.

        skip_not_found : :class:`Optional` [ :class:`bool` ]
            Specify that if the method is not found if it is OK.

        kwargs : :class:`Any`
            Method parameters.

        Returns
        -------
        :class:`Dict` [ :class:`dict`, :class:`Any` ] :
            Result from the method called.

        Raises
        ------
        PluginMethodException
            An exception was raised in the plugin method.

        PluginNotFoundException
            Exception raised when plugin is not found.

        """

        # Loop with plugins, if they have overridden the method, then call it
        results = {}
        for plugin_name, plugin in self.plugins.items():
            # Check if we're going to raise an exception or just skip
            if not hasattr(plugin, method_name):
                if skip_not_found:
                    continue
                raise PluginMethodException(f'Plugin "{plugin_name}" has no method "{method_name}"')
            # Grab the method
            method = getattr(plugin, method_name)
            # Call it
            res = method(**kwargs)
            # Save the result
            results[plugin_name] = res

        return results

    def call_plugin(self, plugin_name: str, method_name: str, **kwargs: Any) -> Any:
        """
        Call a specific plugins' method.

        Parameters
        ----------
        plugin_name : :class:`str`
            Plugin name.

        method_name : :class:`str`
            Method name to call.

        kwargs : Any
            Parameters to pass to the method.

        Returns
        -------
        :class:`Any` :
            Result from plugin.

        Raises
        ------
        PluginMethodException
            An exception was raised in the plugin method.

        PluginNotFoundException
            Exception raised when plugin is not found.

        """

        # Check if plugin exists
        if plugin_name not in self.plugins:
            raise PluginNotFoundException(f'Plugin "{plugin_name}"" not found')
        # If it does then grab it
        plugin = self.plugins[plugin_name]

        # Check if we're going to raise an exception or just skip
        if not hasattr(plugin, method_name):
            raise PluginMethodException(f'Plugin "{plugin_name}" has no method "{method_name}"')
        # Grab the method
        method = getattr(plugin, method_name)
        # Call it
        return method(**kwargs)

    def get(self, plugin_name: str) -> Plugin:
        """
        Return a plugin by name.

        Parameters
        ----------
        plugin_name : :class:`str`
            Plugin name to get.

        Returns
        -------
        :class:`Plugin` :
            Returns the plugin we were looking for.

        Raises
        ------
        PluginNotFoundException
            Exception raised when plugin is not found.

        """

        if plugin_name not in self.plugins:
            raise PluginNotFoundException(f'Plugin "{plugin_name}" not found')

        return self.plugins[plugin_name]

    #
    # Internals
    #

    def _load_plugins(self) -> None:
        """Load plugins from the plugin_package we were provided."""

        # Load plugin packages
        for plugin_package in self._plugin_packages:
            self._find_plugins(plugin_package)

    def _find_plugins(self, package_name: str) -> None:  # pylint: disable=too-many-branches, too-complex
        """Recursively search the plugin_package and retrieve all plugins."""

        imported_package = __import__(package_name, fromlist=["__VERSION__"])

        # Iterate through the modules
        for _, plugin_name, ispkg in pkgutil.iter_modules(imported_package.__path__, imported_package.__name__ + "."):
            # If this is not a package, continue and check the members
            if not ispkg:
                try:
                    plugin_module = __import__(plugin_name, fromlist=["__VERSION__"])
                except ModuleNotFoundError as err:
                    self._plugin_status[plugin_name] = f"cannot load module: {err}"
                    continue

                # Grab object members
                object_members = inspect.getmembers(plugin_module, inspect.isclass)

                # Loop with class names
                for _, class_name in object_members:
                    # Only add classes that are a sub class of Plugin
                    if issubclass(class_name, Plugin) and (class_name is not Plugin) and not class_name.__name__.endswith("Base"):
                        # Save plugin and record that it was loaded
                        self._plugins[plugin_name] = class_name()
                        self._plugin_status[plugin_name] = "loaded"

        # Look for modules in sub packages
        all_current_paths: List[str] = []

        if isinstance(imported_package.__path__, str):
            all_current_paths.append(imported_package.__path__)
        else:
            all_current_paths.extend(imported_package.__path__)

        # Loop with package path
        for pkg_path in all_current_paths:
            # Make sure its not see in our main seen_paths
            if pkg_path not in self._seen_paths:
                # If not add it so we don't process it again
                self._seen_paths.append(pkg_path)

                # Grab all the sub directories of the current package path directory
                sub_dirs = []
                for sub_dir in os.listdir(pkg_path):
                    # If the subdir starts with a ., ignore it
                    if sub_dir.startswith("."):
                        continue
                    # If this is not a sub dir, then move onto the next one
                    if not os.path.isdir(os.path.join(pkg_path, sub_dir)):
                        continue
                    # Add sub-directory
                    sub_dirs.append(sub_dir)

                # Find packages in sub directory
                for sub_dir in sub_dirs:
                    self._find_plugins(f"{package_name}.{sub_dir}")

    @property
    def plugins(self) -> Dict[str, Plugin]:
        """Return the list of plugins registered."""

        return self._plugins

    @property
    def plugin_status(self) -> Dict[str, str]:
        """Return the a dict of all the plugin statuses."""

        return self._plugin_status
