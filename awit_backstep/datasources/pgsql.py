"""AWIT Backstep pipe datasource."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from typing import List, Optional

from . import BackupArchivePipe, BackupSet, DatasourcePluginBase

__all__ = ["PgsqlPlugin"]

__VERSION__ = "0.0.2"


class PgsqlPlugin(DatasourcePluginBase):
    """Postgresql Plugin."""

    _name = "pgsql"

    def get_backup_set(self, backup_items: List[str], backup_name: Optional[str]) -> BackupSet:
        """Parse the backup items into command arguments and create a pipe to Postgresql."""

        # Call the parent object to set things up
        super().get_backup_set(backup_items=backup_items, backup_name=backup_name)

        # Create backup set
        backup_set = BackupSet()

        # Postgresql dump arguments
        pgsql_args = ["sudo", "-u", "postgres", "--", "pg_dump", "--serializable-deferrable"]
        pgsql_args.extend(backup_items)

        pgsql_archive = BackupArchivePipe(
            name=self.archive_basename, filename="dump.sql", args=pgsql_args, notifiers=self.notifiers
        )

        backup_set.add_archive(pgsql_archive)

        return backup_set
