"""AWIT Backstep LibvirtDomain datasource."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import subprocess  # nosec
from datetime import datetime
from typing import Dict, List, Optional, Tuple
from xml.etree import ElementTree  # nosec

import libvirt  # pylint: disable=import-error

from ..notifiers import NotifierList
from ..util import chunks
from . import BackupArchivePaths, BackupSet, DatasourceError, DatasourcePluginBase

__all__ = ["LibvirtBackupArchive", "LibvirtPlugin"]

__VERSION__ = "0.1.5"


class LibvirtBackupArchive(BackupArchivePaths):
    """Libvirt backup archive class."""

    _snapshot_files: Dict[str, str]
    _libvirt_domain: libvirt.virDomain
    _libvirt_domain_name: str
    _disks: Dict[str, Dict[str, str]]

    def __init__(self, libvirt_conn: libvirt.virConnect, archive_name: str, domain_name: str, notifiers: NotifierList):
        """
        Inititalize the object.

        :param libvirt_conn: Connection to libvirt
        :type libvirt_conn: libvirt.virConnect
        :param archive_name: Backup archive name
        :type archive_name: str
        :param domain_name: Libvirt virtual machine domain name we'll be backing up
        :type domain_name: str
        """

        # Use super to initialize the base class
        super().__init__(name=archive_name, notifiers=notifiers)

        # Loopkup the libvirt domain from the domain name we go
        self._libvirt_domain = libvirt_conn.lookupByName(domain_name)
        # Save the actual domain name in libvirt
        self._libvirt_domain_name = self.libvirt_domain.name()

        # Grab our disks
        self._disks = self._get_disks()
        # Snapshot files, once we're run pre-backup
        self._snapshot_files = {}

        # Output info about the disks we're going to backup
        self.notifiers.info(f'Adding VM "{self.libvirt_domain_name}" disk images to archive "{archive_name}":')
        for _, disk in self.disks:
            self.notifiers.info("  - " + ", ".join([f"{x}={y}" for x, y in disk.items()]))

    def pre_backup(self) -> None:
        """Pre-backup method, where we snapshot the disks."""

        # Add disks to path list
        needs_trim = False
        for disk_name, disk in self.disks:
            if disk["type"] in {"qcow2", "raw"}:
                self.notifiers.debug(f'VM "{self.libvirt_domain_name}" disk "{disk_name}" has path "{disk["path"]}"')
                self.add_path(disk["path"])
            if disk["type"] == "qcow2" and disk["supports_discard"] == "yes":
                needs_trim = True

        # FSTRIM the VM
        if needs_trim:
            self._fstrim()

        # Snapshot disks
        self._create_snapshot()

    def post_backup(self) -> None:
        """Post-backup method, where we recover the snapshots."""

        # Commit our snapshot afterwards
        self._commit_snapshot()

    def _create_snapshot(self) -> None:  # pylint: disable=too-complex
        """Create a snapshot."""

        if not self.libvirt_domain.isActive():
            self.notifiers.info(f'VM "{self.libvirt_domain_name}" is not running, skipping snapshot')
            return

        snapshot_name = datetime.now().strftime("awitbackstep-snapshot-%Y%m%d%H%M%S")

        snapshot_args = [
            # We use snapshot-create-as to generate the XML just incase options differe from if we used XML ourselves
            "virsh",
            "snapshot-create-as",
            "--domain",
            self.libvirt_domain_name,
            snapshot_name,
            "--disk-only",
            "--atomic",
            "--quiesce",
            "--no-metadata",
        ]

        # Loop with disks and add them to the snapshot
        for device, disk in self.disks:
            # Make sure disk is qcow2, if not we're not going to be snapshotting
            if disk["type"] != "qcow2":
                self.notifiers.info(
                    f'VM "{self.libvirt_domain_name}" disk "{device}" will NOT be snapshotted as type "'
                    + disk["type"]
                    + '" is not "qcow2"'
                )
                continue

            # Make sure this path has not got our snapshot tag
            if "awitbackstep-snapshot-" in disk["path"]:
                raise DatasourceError(f'VM "{self.libvirt_domain_name}" disk "{device}" already has a snapshot')

            # Create a snapshot file
            snapshot_file = f'{disk["path"]}.{snapshot_name}'
            # Build disk spec
            snapshot_args.append("--diskspec")
            snapshot_args.append(f"{device},file={snapshot_file}")
            # Add snapshot file
            self._snapshot_files[device] = snapshot_file

        self.notifiers.info("Snapshotting started")
        self.notifiers.debug(f"Running: {snapshot_args}")
        try:
            # Create process and monitor status
            process = subprocess.Popen(  # nosec # pylint: disable=consider-using-with
                snapshot_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
            )
            # Loop with output
            for line in chunks(process.stdout):
                self.notifiers.info(f" - {line}")
        except KeyboardInterrupt:
            self.notifiers.error("Snapshotting failed")
            raise DatasourceError(f'Keyboard interrupt while snapshotting VM "{self.libvirt_domain_name}"') from None
        except subprocess.CalledProcessError as err:
            self.notifiers.error("Snapshotting failed")
            raise DatasourceError(
                f'Error running "virsh snapshot-create-as" on VM "{self.libvirt_domain_name}", '
                f"exited with code {err.returncode}"
            ) from None
        except OSError as err:
            self.notifiers.error("Snapshotting failed")
            raise DatasourceError(
                f'OS error running "virsh snapshot-create-as" on VM "{self.libvirt_domain_name}", ' f"exited with => {err}"
            ) from None
        # Check result code
        process.communicate()
        result_code = process.poll()
        if result_code:
            self.notifiers.error("Snapshotting failed")
            raise DatasourceError(
                f'Error running "virsh snapshot-create-as" on VM "{self.libvirt_domain_name}", ' f"exited with code {result_code}"
            ) from None

        self.notifiers.info("Snapshotting done")

    def _fstrim(self) -> None:
        """Notify the domain to start an FSTRIM."""

        if not self.libvirt_domain.isActive():
            self.notifiers.info(f'VM "{self.libvirt_domain_name}" is not running, skipping FSTRIM')
            return

        # The below will basically test if we can communicate to the guest, so we trap any errors
        try:
            self.notifiers.info("FSTRIM started")
            self.libvirt_domain.fSTrim(None, 0)
            self.notifiers.info("FSTRIM done")
        except libvirt.libvirtError as err:
            self.notifiers.error("FSTRIM failed")
            raise DatasourceError(f"Libvirt error during FSTRIM: {err}") from None

    def _commit_snapshot(self) -> None:
        """Commit a domain snapshot."""

        if not self.libvirt_domain.isActive():
            self.notifiers.info(f'VM "{self.libvirt_domain_name}" is not running, skipping snapshot commit')
            return

        # Loop with snapshotted devices
        for device, snapshot_filename in sorted(self._snapshot_files.items()):
            # Try do a commit
            try:  # pylint: disable=too-many-try-statements
                self.notifiers.info(f'Starting snapshot commit for disk "{device}"')
                commit_args = ["virsh", "blockcommit", self.libvirt_domain_name, device, "--pivot", "--verbose"]
                self.notifiers.debug(f"Running: {commit_args}")
                # Create process and monitor status
                process = subprocess.Popen(  # nosec # pylint: disable=consider-using-with
                    commit_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                )
                # Loop with output
                for line in chunks(process.stdout, delim="\n\r"):
                    self.notifiers.info(f" - {line}")
            except KeyboardInterrupt:
                self.notifiers.error("Snapshot commit failed")
                raise DatasourceError(f'Keyboard interrupt during snapshot commit VM "{self.libvirt_domain_name}"') from None
            except subprocess.CalledProcessError as err:
                self.notifiers.error("Snapshot commit failed")
                raise DatasourceError(
                    f'Error running "virsh blockcommit" on VM "{self.libvirt_domain_name}", ' f"exited with code {err.returncode}"
                ) from None
            except OSError as err:
                self.notifiers.error("Snapshot commit failed")
                raise DatasourceError(
                    f'OS error running "virsh blockcommit" on VM "{self.libvirt_domain_name}", ' f"exited with => {err}"
                ) from None
            # Check result code
            process.communicate()
            result_code = process.poll()
            if result_code:
                self.notifiers.error("Snapshotting failed")
                raise DatasourceError(
                    f'Error running "virsh snapshot-create-as" on VM "{self.libvirt_domain_name}", '
                    f"exited with code {result_code}"
                ) from None

            self.notifiers.info(f'Completed snapshot commit for disk "{device}"')

            # Remove dangling snapshot file
            self.notifiers.info("Removing snapshot file")
            os.unlink(snapshot_filename)
            del self._snapshot_files[device]

    def _get_disks(self) -> Dict[str, Dict[str, str]]:
        """Get all the domains disk info."""
        # Grab root node
        root = ElementTree.fromstring(self.libvirt_domain.XMLDesc())  # nosec

        # Search <disk type='file' device='disk'> entries
        disks = root.findall("./devices/disk")

        # For every disk get drivers, sources and targets
        drivers: List[Dict[str, str]] = []
        sources: List[Dict[str, str]] = []
        targets: List[Dict[str, str]] = []
        for disk in disks:
            # Grab driver, source and target
            driver = disk.find("driver")
            source = disk.find("source")
            target = disk.find("target")

            # Make sure we got all of them
            if driver is None or source is None or target is None:
                raise RuntimeError(f"VM disk has missing info: {driver=}, {source=}, {target=}")
            # Append to lists
            drivers.append(driver.attrib)
            sources.append(source.attrib)
            targets.append(target.attrib)

        # Create a list of our disks
        disks_info: Dict[str, Dict[str, str]] = {}
        for i, disk_source in enumerate(sources):
            disk_info: Dict[str, str] = {}
            disk_info["device"] = targets[i]["dev"]
            disk_info["bus"] = targets[i]["bus"]
            if "dev" in disk_source:
                disk_info["path"] = disk_source["dev"]
            elif "file" in disk_source:
                disk_info["path"] = disk_source["file"]
            else:
                raise RuntimeError('Failed to find "dev" or "file" in sources[i]')

            disk_info["type"] = drivers[i]["type"]
            # Check if we support discard
            if ("discard" in drivers[i]) and (drivers[i]["discard"] == "unmap"):
                disk_info["supports_discard"] = "yes"
            else:
                disk_info["supports_discard"] = "no"
            # Add disk to list
            disks_info[disk_info["device"]] = disk_info

        return disks_info

    @property
    def disks(self) -> List[Tuple[str, Dict[str, str]]]:
        """Return a list of our disks."""
        return sorted(self._disks.items())

    @property
    def libvirt_domain(self) -> libvirt.virDomain:
        """Libvirt domain property."""
        return self._libvirt_domain

    @property
    def libvirt_domain_name(self) -> str:
        """Libvirt domain name property."""
        return self._libvirt_domain_name


class LibvirtPlugin(DatasourcePluginBase):
    """Libvirt Plugin."""

    _name = "libvirt"

    def get_backup_set(self, backup_items: List[str], backup_name: Optional[str]) -> BackupSet:
        """Parse the backup items into backup archives and paths."""

        # Call the parent object to set things up
        super().get_backup_set(backup_items=backup_items, backup_name=backup_name)

        # Create backup set
        backup_set = BackupSet()

        # Grab connection to libvirt
        self.notifiers.info("Connecting to libvirtd")
        libvirt_conn = libvirt.open("qemu:///system")
        self.notifiers.info("Connected to libvirtd")

        # Loop with each VM and add the backup archive for it, containing all the disks...
        for vm_name in backup_items:
            archive_name = f"{self.archive_basename}-{vm_name}"
            # Create the backup archive
            try:
                libvirt_archive = LibvirtBackupArchive(
                    libvirt_conn=libvirt_conn, archive_name=archive_name, domain_name=vm_name, notifiers=self.notifiers
                )
                # Add add it to the set
                backup_set.add_archive(libvirt_archive)

            except libvirt.libvirtError as err:
                self.notifiers.error(f"libvirt error: {err}")

        return backup_set
