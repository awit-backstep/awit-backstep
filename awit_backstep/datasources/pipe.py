"""AWIT Backstep pipe datasource."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from typing import List, Optional

from . import BackupArchivePipe, BackupSet, DatasourceError, DatasourcePluginBase

__all__ = ["PipePlugin"]

__VERSION__ = "0.0.1"


class PipePlugin(DatasourcePluginBase):
    """Pipe Plugin."""

    _name = "pipe"

    def get_backup_set(self, backup_items: List[str], backup_name: Optional[str]) -> BackupSet:
        """Parse the backup items into command arguments and create a pipe."""

        # Call the parent object to set things up
        super().get_backup_set(backup_items=backup_items, backup_name=backup_name)

        # Create backup set
        backup_set = BackupSet()

        # Make sure we have a filename and a command
        if len(backup_items) < 2:
            raise DatasourceError(
                "For the 'pipe' datasource at least the following positional arguments are required: <filename> <command>"
            )

        pipe_archive = BackupArchivePipe(
            name=self.archive_basename, filename=backup_items[0], args=backup_items[1:], notifiers=self.notifiers
        )

        backup_set.add_archive(pipe_archive)

        return backup_set
