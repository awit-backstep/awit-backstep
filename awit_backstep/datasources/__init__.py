"""AWIT Backstep datasources."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess  # nosec
import threading
from typing import Any, List, Optional

from ..notifiers import NotifierList
from ..plugin import Plugin
from ..util import chunks

__all__ = [
    "BackupArchiveBase",
    "BackupArchivePipe",
    "BackupArchivePaths",
    "BackupArchiveDevices",
    "BackupSet",
    "DatasourcePluginBase",
    "DatasourceError",
]


class DatasourceError(RuntimeError):
    """Datasource error exception."""


class BackupArchiveBase:
    """Base for our backup archives."""

    _notifiers: NotifierList

    def __init__(self, name: str, notifiers: Optional[NotifierList] = None):
        """
        Create a backup archive.

        :param name: Name of the backup archive
        :type name: str
        :param notifiers: Notifiers to use for logging
        :type notifiers: NotifierList
        """

        if notifiers is None:
            raise RuntimeError("notifiers cannot be None")

        self._name = name
        self._notifiers = notifiers

    def pre_backup(self) -> None:
        """Prepare the backup archive for backup."""

    def post_backup(self) -> None:
        """Cleanup the backup archive after backup."""

    @property
    def name(self) -> str:
        """Return the name of this archive."""
        return self._name

    @property
    def notifiers(self) -> NotifierList:
        """Return our notifiers."""
        return self._notifiers

    @notifiers.setter
    def notifiers(self, notifiers: NotifierList) -> None:
        """Set our notifiers."""
        self._notifiers = notifiers


class BackupArchivePipe(BackupArchiveBase):
    """
    Create a backup archive whose source is a pipe.

    :param name: Name of the backup archive
    :type name: str
    :param args: List of process arguments
    :type paths: List[str]
    """

    _name: str
    _args: List[str]
    _filename: str
    _process: Optional[subprocess.Popen[Any]]
    _result_code: Optional[int]
    _logger_thread: Optional[threading.Thread]

    def __init__(self, name: str, filename: str, args: List[str], notifiers: NotifierList):
        """
        Create a backup archive.

        :param name: Name of the backup archive
        :type name: str
        :param args: List of process arguments
        :type args: List[str]
        """

        # Call parent class and initialize our name and notifiers
        super().__init__(name=name, notifiers=notifiers)

        self._args = []
        self._filename = filename
        self._process = None
        self._result_code = None
        self._logger_thread = None

        if args:
            self.add_args(args)

    def add_arg(self, arg: str) -> None:
        """
        Add an arg to the process we're going to be executing.

        Parameters
        ----------
        arg : :class:`str`
            Path to add.

        """
        self._args.append(arg)

    def add_args(self, args: List[str]) -> None:
        """
        Add a list of args to the process we're going to be executing.

        :param args: Arg to add
        :type args: List[str]
        """
        self._args.extend(args)

    def pre_backup(self) -> None:
        """Prepare the backup archive for backup by creating the process."""

        self.notifiers.debug(f"Running pipe-process: {self.args}")
        # Create process
        self._process = subprocess.Popen(  # nosec # pylint: disable=consider-using-with
            self.args, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        # Fire up STDERR reader thread
        self._logger_thread = threading.Thread(target=self._process_logger, kwargs={"process": self._process})
        self._logger_thread.start()

    def _process_logger(self, process: subprocess.Popen[Any]) -> None:
        """Process logger for a pipe."""

        # Loop with output
        for line in chunks(process.stderr, delim="\n\r"):
            self.notifiers.info(f"pipe-process: {line}")

    def post_backup(self) -> None:
        """Cleanup the backup archive after backup by setting the result code."""

        if not self.process:
            raise RuntimeError("No active process")

        if not self._logger_thread:
            raise RuntimeError("No logger thread active")

        # Try communicate with the subproces to see if its ok...
        self.process.communicate()
        # Now poll for an exit status
        self._result_code = self.process.poll()
        self._logger_thread.join()

    @property
    def args(self) -> List[str]:
        """Return the process args associated with this archive."""
        return self._args

    @property
    def filename(self) -> str:
        """Return the filename we're going to use for the piped data."""
        return self._filename

    @property
    def process(self) -> Optional[subprocess.Popen[Any]]:
        """Return the process."""
        return self._process

    @property
    def result_code(self) -> Optional[int]:
        """Return the process."""
        return self._result_code


class BackupArchivePaths(BackupArchiveBase):
    """
    Create a backup archive.

    :param name: Name of the backup archive
    :type name: str
    :param paths: Optional list of filesystem paths to initialize the archive with
    :type paths: List[str]
    """

    _name: str
    _paths: List[str]

    def __init__(self, name: str, paths: Optional[List[str]] = None, notifiers: Optional[NotifierList] = None):
        """
        Create a backup archive.

        :param name: Name of the backup archive
        :type name: str
        :param paths: Optional list of filesystem paths to initialize the archive with
        :type paths: List[str]
        """

        # Call parent class and initialize our name and notifiers
        super().__init__(name=name, notifiers=notifiers)

        self._paths = []

        if paths:
            self.add_paths(paths)

    def add_path(self, path: str) -> None:
        """
        Add a path to the backup archive.

        :param path: Path to add
        :type path: str
        """
        self._paths.append(path)

    def add_paths(self, paths: List[str]) -> None:
        """
        Add a list of paths to the backup archive.

        :param paths: Path to add
        :type paths: List[str]
        """
        self._paths.extend(paths)

    @property
    def paths(self) -> List[str]:
        """Return the paths associated with this archive."""
        return self._paths


class BackupArchiveDevices(BackupArchivePaths):
    """
    Create a backup archive that contains devices.

    :param name: Name of the backup archive
    :type name: str
    :param paths: Optional list of device paths to initialize the archive with
    :type paths: List[str]
    """


class BackupSet:
    """Backup set class."""

    _archives: List[BackupArchiveBase]

    def __init__(self) -> None:
        """Backup set class."""
        self._archives = []

    def add_archive(self, archive: BackupArchiveBase) -> None:
        """
        Add an archive to the backup set.

        :param archive: Backup archive to add
        :type archive: BackupArchive
        """
        self._archives.append(archive)

    @property
    def archives(self) -> List[BackupArchiveBase]:
        """Return the archives associated with this backup set."""
        return self._archives


class DatasourcePluginBase(Plugin):
    """Datasource plugin."""

    _notifiers: Optional[NotifierList]
    _name: str
    _archive_basename: Optional[str]

    def __init__(self) -> None:
        """Initialize the object."""

        super().__init__()

        self._notifiers = None
        self._archive_basename = None

    def get_backup_set(self, backup_items: List[str], backup_name: Optional[str]) -> BackupSet:  # pylint: disable=unused-argument
        """Parse backup items into a list of paths."""

        # Work out archive name...
        if backup_name:
            self._archive_basename = f"{backup_name}-{self.name}"
        else:
            self._archive_basename = self.name

        return BackupSet()

    def register_datasource(self, **kwargs: Any) -> str:  # pylint: disable=unused-argument
        """Register the datasource."""
        return self.name

    @property
    def name(self) -> str:
        """
        Name property.

        :returns: Name of notifier
        :rtype: str
        """
        return self._name

    @property
    def archive_basename(self) -> str:
        """Return our archive_basename property."""

        if not self._archive_basename:
            raise RuntimeError("archive_basename used but not set")

        return self._archive_basename

    @property
    def notifiers(self) -> NotifierList:
        """Return our notifiers."""

        if not self._notifiers:
            raise RuntimeError("_notifiers accessed but not set")

        return self._notifiers

    @notifiers.setter
    def notifiers(self, notifiers: NotifierList) -> None:
        """Set our notifiers."""
        self._notifiers = notifiers
