"""AWIT Backstep main package."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import configparser
import os
import os.path
import random
import re
import sys
import time
from typing import Any, Dict, List, NoReturn, Optional, Sequence

from .datasources import DatasourcePluginBase
from .notifiers import NotifierList, NotifierPluginBase
from .plugin import PluginCollection
from .version import __version__

__all__ = ["Backstep", "BackstepArgumentParserException", "BackstepArgumentParser"]

CONFIG_FILE = "/etc/awit-backstep/auto-backup.ini"
CONFIG_FILE_USER = "~/.config/awit-backstep/auto-backup.ini"
PID_FILE = "/run/awit-backstep.lock"


class BackstepArgumentParserException(RuntimeError):
    """Exception raised when parsing arguments."""


class BackstepArgumentParser(argparse.ArgumentParser):
    """
    Backstep argument parser.

    We need to override the error handling so it works for the configuration file options too instead of printing out help.
    """

    def error(self, message: str) -> NoReturn:
        """Error exception generator."""
        raise BackstepArgumentParserException(message)

    def error_cmdline(self, message: str) -> None:
        """Invoke the super error handler."""
        super().error(message)


class Backstep:  # pylint: disable=too-many-instance-attributes
    """Main Backstep application object."""

    # Internal plugin collections
    _datasource_plugins: PluginCollection
    _method_plugins: PluginCollection
    _notifier_plugins: PluginCollection

    # Mapping of the datasource and notifier name to the package name
    _datasource_mapping: Dict[str, str]
    _notifier_mapping: Dict[str, str]

    # Mapping of actions (first positional argument), to package names
    _actions: Dict[str, str]

    # This is the datasource and notifier list we're currently using
    _datasource: Optional[DatasourcePluginBase]
    _notifiers: NotifierList

    # Argument parser
    _parser: BackstepArgumentParser

    def __init__(self) -> None:
        """Initialize object."""

        # Grab our plugins
        self._datasource_plugins = PluginCollection(["awit_backstep.datasources"])
        self._method_plugins = PluginCollection(["awit_backstep.methods"])
        self._notifier_plugins = PluginCollection(["awit_backstep.notifiers"])
        # Map in datasources and notifiers
        self._datasource_mapping = {}
        self._notifier_mapping = {}
        # We start off with no command actions...
        self._actions = {}
        # Active datasource and notifier list we're using
        self._datasource = None
        self._notifiers = NotifierList()
        # Start off with a clean argument parser
        prog: Optional[str] = None
        if sys.argv[0].endswith("__main__.py"):
            prog = "python -m awit_backstep"
        self._parser = BackstepArgumentParser(prog=prog)

        # Add mappings between the name and package name
        self._map_datasources()
        self._map_notifiers()
        # Register argument parsers
        self._register_parsers()

    def run(self) -> None:
        """Run Backstep."""

        # Parse args
        try:
            command_args = self._parse_args()
        except BackstepArgumentParserException as err:
            # Print out the normal commandline error (and exit)
            self._parser.error_cmdline(err.args[0])
            # This is never reached, its only here to make linting happy
            return

        # Only enable traceback in DEBUG mode
        if command_args["verbose"] != "DEBUG":
            sys.tracebacklimit = 0

        self._notifiers.info(f"AWIT-Backstep v{__version__} - Copyright 2019-2023, AllWorldIT")

        # Execute plugin responsible for this action
        if command_args["action"] == "auto-backup":
            extra_args = []
            # Inititalize options
            options = {
                # Add the random delay time
                "random_delay": command_args["random_delay"]
            }
            # Check if we using any extra args
            if ("check_repair" in command_args) and (command_args["check_repair"]):
                extra_args.append("--check-repair")
            if ("compact" in command_args) and (command_args["compact"]):
                extra_args.append("--compact")
            if ("only" in command_args) and (command_args["only"]):
                options["only"] = command_args["only"]
            self._parse_config_file(os.path.expanduser(command_args["config"]), extra_args, options)
        elif command_args["action"] == "show-plugins":
            self._show_plugins()
        else:
            self.execute_plugin(command_args["action"], command_args)

    def _register_parsers(self) -> None:
        """Register parsers."""

        # Create argument parser
        subparsers = self._parser.add_subparsers()

        # Create argument parser for datasource
        datasource_parser = argparse.ArgumentParser(add_help=False)
        datasource_parser.add_argument(
            "--datasource", required=True, choices=self._datasource_mapping.keys(), help="Datasource plugin to use"
        )
        # Create argument parser for notifier
        notifier_parser = argparse.ArgumentParser(add_help=False)

        # Add automated backup parser
        autobackup_parser = subparsers.add_parser("auto-backup", description="Command: auto-backup")
        autobackup_parser.add_argument(
            "--action", action="store_const", const="auto-backup", default="auto-backup", help=argparse.SUPPRESS
        )
        # Work out default configuration file depending if we're root or not
        config_file_default = CONFIG_FILE_USER if os.geteuid() else CONFIG_FILE
        autobackup_parser.add_argument(
            "--config",
            metavar="CONFIG_FILE",
            default=config_file_default,
            help=f'Automated backup configuration file (default: "{config_file_default}")',
        )
        autobackup_parser.add_argument("--check-repair", action="store_true", help="Do a check --repair first (if supported)")
        autobackup_parser.add_argument("--compact", action="store_true", help="Do a --compact once done")
        autobackup_parser.add_argument("--only", nargs="+", help="Only run for this one backup")
        autobackup_parser.add_argument(
            "--random-delay",
            type=int,
            default=0,
            help="Random number of minutes to delay before starting automatic backups. Only applies to 'auto-backup'. (default: 0)",
        )

        # Add automated backup parser
        showplugins_parser = subparsers.add_parser("show-plugins", description="Command: show-plugins")
        showplugins_parser.add_argument(
            "--action", action="store_const", const="show-plugins", default="show-plugins", help=argparse.SUPPRESS
        )

        # Call each notifier plugin to register additonal parser options
        self._notifier_plugins.call("register_parser", notifier_parser=notifier_parser)

        # Call each method plugin to register parsers
        for plugin_name, plugin_actions in self._method_plugins.call(
            "register_parser", subparsers=subparsers, datasource_parser=datasource_parser, notifier_parser=notifier_parser
        ).items():
            # Loop with each command returned and add a link to the plugin action
            for action in plugin_actions:
                self._actions[action] = plugin_name

        # Add logging level option
        self._parser.add_argument(
            "-v",
            dest="verbose",
            default="INFO",
            type=str,
            choices=["DEBUG", "INFO", "NOTICE", "WARNING", "ERROR", "CRITICAL"],
            help="Logging level",
        )

    def _parse_args(self, args: Optional[Sequence[str]] = None, notifiers: Optional[NotifierList] = None) -> Dict[str, Any]:
        """Parse the commandline arguments."""

        # Parse commandline arguments
        command_args = vars(self._parser.parse_args(args))

        # If we don't have an action, we should display our usage
        if "action" not in command_args:
            self._parser.print_usage()
            sys.exit(1)

        # Save the datasource if one was specified on the commandline
        if "datasource" in command_args:
            self._datasource = self._datasource_from_name(command_args["datasource"])

        _use_notifiers = notifiers if notifiers else self._notifiers

        # Check which notifiers we'll be using
        self._notifier_plugins.call("use_if_enabled", command_args=command_args, notifiers=_use_notifiers)

        # Set notifier verbosity if its still default, else we keep what we have
        # NK: This means what if we're doing an auto-backup, we get the verbosity from the main invocation of backstep
        if _use_notifiers.is_default_verbosity:
            _use_notifiers.set_verbosity(command_args["verbose"])

        return command_args

    def _parse_config_file(  # pylint: disable=too-many-locals,too-complex
        self, config_file: str, extra_args: Sequence[str], options: Dict[str, Any]
    ) -> None:
        """Parse config file into command_args structure."""

        if not os.path.exists(config_file):
            raise RuntimeError(f'ERROR: Config file "{config_file}" not found')

        self._notifiers.info(f'Using configuration from file "{config_file}')
        parser = configparser.ConfigParser(allow_no_value=True)
        # Try load config file
        try:
            parser.read(config_file)
        except configparser.Error as err:
            raise RuntimeError(f"ERROR: Failed to parse config file: {err}") from None

        # Check if we have a random delay time to start the backups
        if options["random_delay"]:
            delay_time = random.randint(0, options["random_delay"])  # nosec
            self._notifiers.notice(f"Delaying backup by a random time period of {delay_time} mins...")
            time.sleep(delay_time * 60)

        # Loop with each section, which are our BACKUP_NAME's below
        for section in parser.sections():
            # This is the arguments we'll pass to our own parser
            auto_args: List[str] = []
            # This is the list of backup items added to the end of auto_args
            backup_items: List[str] = []

            # If we are only running for a certain section, then skip others
            if ("only" in options) and (section not in options["only"]):
                only = options["only"]
                self._notifiers.debug(f'>>> Skipping automated backup for: {section} (only "{only}")')
                continue

            self._notifiers.debug(f"  Backup: {section}")

            # Loop with each key/value pair and generate arguments
            for key, value_raw in parser.items(section):
                # Replace variables
                value = value_raw.replace("$BACKUP_NAME", section)
                # Check for things handled specially
                if key == "method":
                    auto_args.insert(0, value)
                    continue
                if key == "backup-items":
                    backup_items = re.split(r"\n+", value.strip())
                    continue
                # If the value is 'enabled' we remove it and just add the key as an option
                if value == "enabled":
                    auto_args.append(f"--{key}")
                else:
                    auto_args.extend([f"--{key}", value])
                # Log what we found
                self._notifiers.debug(f"    {key} = {value}")

            # Add backup-name if its not already added, this allows us to override it in the auto-backup config file
            if "--backup-name" not in auto_args:
                auto_args.extend(["--backup-name", section])

            # We may have more args to add
            auto_args.extend(extra_args)

            # Add backup items to the end of the arg list
            auto_args.extend(backup_items)

            self._notifiers.debug(f"Parsed config file args: {auto_args}")

            # Work out a list of notifiers for this backup...
            notifiers = NotifierList(verbosity=self._notifiers.verbosity)

            # Try and parse the arguments we built
            try:
                command_args = self._parse_args(args=auto_args, notifiers=notifiers)
            except BackstepArgumentParserException as err:
                # Clean up message (replace XXX-- with XXX=)
                message = re.sub(r"--([0-9a-z\-]+)", r"\1=", err.args[0])
                # Change wording slightly
                message = re.sub(r"arguments(:| are)", r"options\1", message)
                print(f"ERROR: auto-backup failed => {message}")
                sys.exit(1)

            self._notifiers.notice(f">>> Running automated backup for: {section}")
            self._notifiers.debug(f"ARGS: {command_args}")

            # Execute the plugin
            self.execute_plugin(action=command_args["action"], command_args=command_args, notifiers=notifiers)

    def _map_datasources(self) -> None:
        """Map our datasources."""

        for plugin_name, datasource in self._datasource_plugins.call("register_datasource").items():
            self._datasource_mapping[datasource] = plugin_name

    def _map_notifiers(self) -> None:
        """Map our notifiers."""

        for plugin_name, notifier in self._notifier_plugins.call("register_notifier").items():
            self._notifier_mapping[notifier] = plugin_name

    def _datasource_from_name(self, name: str) -> DatasourcePluginBase:
        """
        Return a datasource from its' name.

        :param name: Name of the datasource
        :type name: str
        """

        # Make sure the datasource exists
        if name not in self._datasource_mapping:
            raise RuntimeError(f'ERROR: Datasource "{name}" not found')

        # Grab datasource by name
        datasource = self._datasource_plugins.get(self._datasource_mapping[name])
        # Make sure its the right type
        if not isinstance(datasource, DatasourcePluginBase):
            raise RuntimeError("Datasource class mismatch")

        return datasource

    def _notifier_from_name(self, name: str) -> NotifierPluginBase:
        """
        Return a notifier from its' name.

        :param name: Name of the notifier
        :type name: str
        """

        # Make sure the notifier exists
        if name not in self._notifier_mapping:
            raise RuntimeError(f'ERROR: Notifier "{name}" not found')

        # Grab notifier by name
        notifier = self._notifier_plugins.get(self._notifier_mapping[name])
        # Make sure its the right class
        if not isinstance(notifier, NotifierPluginBase):
            raise RuntimeError("Notifier class mismatch")

        return notifier

    def _show_plugins(self) -> None:
        """Display a plugin summary of what is loaded and what not."""

        print("PLUGINS STATUS")
        print("\n  Notifiers:")
        for plugin, status in self._notifier_plugins.plugin_status.items():
            print(f"    {plugin}: {status}")
        print("\n  Datasources:")
        for plugin, status in self._datasource_plugins.plugin_status.items():
            print(f"    {plugin}: {status}")
        print("\n  Methods:")
        for plugin, status in self._method_plugins.plugin_status.items():
            print(f"    {plugin}: {status}")

    def execute_plugin(self, action: str, command_args: Dict[str, str], notifiers: Optional[NotifierList] = None) -> None:
        """
        Execute plugin action.

        Parameters
        ----------
        action : :class:`str`
            Action name to call, this is registered on the commandline as a positional argument.

        command_args : :class:`Dict` [ :class:`str` , :class:`str` ]
            Arguments to pass to the plugin.

        notifiers : :class:`Optional` [ :class:`NotifierList` ]
            Optional notifiers to use.

        Raises
        ------
        RuntimeError
            Raised when the plugin has no run method.

        """

        # Grab the action we're going to execute
        method = self._method_plugins.get(self._actions[action])

        # Check which notifiers we're going to use
        _use_notifiers = notifiers if notifiers else self._notifiers

        # Make sure we have a run attribute
        run = getattr(method, "run")  # noqa: B009
        if not run:
            raise RuntimeError("Plugin has no run method")

        # Run the backup method
        run(command=action, notifiers=_use_notifiers, command_args=command_args, datasource=self._datasource)


# Main entry point from the commandline
def main() -> None:
    """Entry point function for the commandline."""

    print(f"AWIT-Backstep v{__version__} - Copyright 2019-2023, AllWorldIT")
    backstep = Backstep()
    backstep.run()
