"""AWIT Backstep destinations."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import argparse
from typing import Any, Dict, List, Optional

from ..datasources import DatasourcePluginBase
from ..notifiers import NotifierList
from ..plugin import Plugin

__all__ = ["MethodPluginBase"]


class MethodPluginBase(Plugin):
    """Method plugin."""

    def register_parser(
        self, subparsers: argparse.Namespace, datasource_parser: argparse.ArgumentParser, notifier_parser: argparse.ArgumentParser
    ) -> List[str]:
        """
        Register command line parsers.

        :param subparsers: argparse subparsers
        :type subparsers: argparse.Namespace
        :param datasource_parser: Datasource parser, used to add as a parent
        :type datasource_parser: argparse.ArgumentParser
        :param notifier_parser: Notifier parser, used to add as a parent
        :type notifier_parser: argparse.ArgumentParser
        :returns: list of commands we support
        :rtype: List[str]
        """
        raise NotImplementedError

    def run(
        self, command: str, notifiers: NotifierList, command_args: Dict[str, Any], datasource: Optional[DatasourcePluginBase] = None
    ) -> None:
        """
        Run plugin.

        Parameters
        ----------
        command : :class:`str`
            Command to run.

        notifiers : :class:`NotifierList`
            Notifier list to use.

        command_args : :class:`Dict` [ :class:`str`, :class:`Any` ]
            Command arguments.

        datasource : :class:`Optional` [ :class:`DatasourcePluginBase` ]
            Optional datasource plugin to use.

        Raises
        ------
        RuntimeError
            Raised if the plugin encounters a runtime error.

        """

        raise NotImplementedError
