"""AWIT Backstep Borg method."""

#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2019-2023, AllWorldIT.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=C0302

import argparse
import contextlib
import json
import socket
import subprocess  # nosec
import time
from datetime import datetime
from timeit import default_timer as timer
from typing import Any, Dict, List, Optional

from ..datasources import (
    BackupArchiveBase,
    BackupArchiveDevices,
    BackupArchivePaths,
    BackupArchivePipe,
    DatasourceError,
    DatasourcePluginBase,
)
from ..notifiers import NotifierList
from ..stats import BackstepStats
from ..util import chunks, human_bytes, json_loads_stream
from . import MethodPluginBase

__all__ = ["BorgBackup", "BorgPlugin"]

__VERSION__ = "0.4.1"


class BackupError(RuntimeError):
    """Backup error exception."""


class BorgBackup:  # pylint: disable=too-many-instance-attributes
    """Borg class."""

    # Setup our properties
    _notifier: NotifierList
    _borg_path: str
    _compression: str
    _server: str
    _server_portknock: Optional[str]
    _server_port: Optional[str]
    _server_user: Optional[str]
    _passphrase_file: Optional[str]
    _ssh_key: str
    _repo: str

    def __init__(self, notifiers: NotifierList, **kwargs: Any) -> None:
        """Initialze the object."""

        self._notifiers = notifiers

        self._borg_path = kwargs.get("borg_path", "borg")

        self._compression = kwargs.get("compression", "zstd")

        # Check that 'server' was specified
        if "server" not in kwargs:
            raise RuntimeError('Borg "server" must be specified')
        self._server = kwargs.get("server", "")

        self._server_portknock = kwargs.get("server_portknock")

        self._server_port = kwargs.get("server_port")
        self._server_user = kwargs.get("server_user")

        self._passphrase_file = kwargs.get("passphrase_file")

        # Check that 'ssh_key' was specified
        if "ssh_key" not in kwargs:
            raise RuntimeError('Borg "ssh_key" must be specified')
        self._ssh_key = kwargs.get("ssh_key", "")

        # Check that 'repo' was specified
        if "repo" not in kwargs:
            raise RuntimeError('Borg "repo" must be specified')
        self._repo = kwargs.get("repo", "")

    def backup(self, archive_name: str, backup_archive: BackupArchiveBase) -> BackstepStats:
        """Start backup using Borg."""

        args_before = ["create", "--progress", "--log-json", "--json"]

        if self.compression:
            args_before.extend(("--compression", self.compression))

        # Check devices first as its a subclass of paths
        if isinstance(backup_archive, BackupArchiveDevices):
            # For raw devices we need read-special enabled
            args_before.append("--read-special")
            return self.run(archive_name, args_before=args_before, args_after=backup_archive.paths)

        if isinstance(backup_archive, BackupArchivePaths):
            return self.run(archive_name, args_before=args_before, args_after=backup_archive.paths)

        if isinstance(backup_archive, BackupArchivePipe):
            # Set the pipe filename instead of getting the default of 'stdin'
            args_before.extend(("--stdin-name", backup_archive.filename))
            return self.run(archive_name, args_before=args_before, args_after=["-"], pipe_process=backup_archive.process)

        raise BackupError("Unknown backup archive passed")

    def backup_init(self) -> BackstepStats:
        """Initialize backup repository."""

        args_before = ["init", "--encryption", "keyfile-blake2"]

        return self.run(args_before=args_before)

    def check_repair(self) -> BackstepStats:
        """Check repair."""

        args_before = ["delete", "--cache-only"]
        self.run(args_before=args_before)

        args_before = ["check", "--repair"]
        return self.run(args_before=args_before, env={"BORG_CHECK_I_KNOW_WHAT_I_AM_DOING": "YES"})

    def compact(self) -> BackstepStats:
        """Compact backups."""

        args_before = ["compact", "--verbose", "--progress"]
        return self.run(args_before=args_before)

    def backup_extract(self, command_args: Dict[str, Any]) -> BackstepStats:
        """Extract a backup from a repository."""

        args_before = ["extract", "--verbose", "--progress"]

        args_after = []
        if "exclude" in command_args and command_args["exclude"]:
            args_after.extend(["--exclude", command_args["exclude"][0]])
        if "pattern" in command_args and command_args["pattern"]:
            args_after.extend(["--pattern", command_args["pattern"][0]])
        if "strip_components" in command_args and command_args["strip_components"]:
            args_after.extend(["--strip-components", str(command_args["strip_components"][0])])
        if "path" in command_args and command_args["path"]:
            args_after.extend(command_args["path"])

        return self.run(args_before=args_before, args_after=args_after)

    def backup_list(self, command_args: Dict[str, Any]) -> BackstepStats:
        """List backups in repository."""

        args_before = ["list"]

        args_after = []
        if "glob_archives" in command_args and command_args["glob_archives"]:
            args_after.extend(["--glob-archives", command_args["glob_archives"][0]])
        if "sort_by" in command_args and command_args["sort_by"]:
            args_after.extend(["--sort-by", command_args["sort_by"][0]])
        if "first" in command_args and command_args["first"]:
            args_after.extend(["--first", str(command_args["first"][0])])
        if "last" in command_args and command_args["last"]:
            args_after.extend(["--last", str(command_args["last"][0])])
        if "exclude" in command_args and command_args["exclude"]:
            args_after.extend(["--exclude", command_args["exclude"][0]])
        if "pattern" in command_args and command_args["pattern"]:
            args_after.extend(["--pattern", command_args["pattern"][0]])

        return self.run(args_before=args_before, args_after=args_after)

    def key_import(self, key_file: str) -> BackstepStats:
        """Import key into Borg."""
        return self.run(args_before=["key", "import", "--verbose"], args_after=[key_file])

    def backup_delete(self, archive_name: str, command_args: Dict[str, Any]) -> BackstepStats:
        """Delete backup from Borg."""

        args_before = ["delete", "--stats"]

        args_after = []
        if "glob_archives" in command_args and command_args["glob_archives"]:
            args_after.extend(["--glob-archives", command_args["glob_archives"][0]])
        if "sort_by" in command_args and command_args["sort_by"]:
            args_after.extend(["--sort-by", command_args["sort_by"][0]])
        if "first" in command_args and command_args["first"]:
            args_after.extend(["--first", str(command_args["first"][0])])
        if "last" in command_args and command_args["last"]:
            args_after.extend(["--last", str(command_args["last"][0])])

        return self.run(archive_name, args_before=args_before, args_after=args_after)

    def backup_prune(  # pylint: disable=too-many-arguments  # noqa: CFQ002
        self,
        archive_prefix: str,
        daily: Optional[str] = None,
        weekly: Optional[str] = None,
        monthly: Optional[str] = None,
        yearly: Optional[str] = None,
        within: Optional[str] = None,
    ) -> BackstepStats:
        """Prune backups."""

        args_before = ["prune", "--list", "--stats"]

        if not (daily or weekly or monthly or within):
            self.notifiers.warning("  - Cannot prune, no retention specified!")
            return {}

        if daily:
            args_before.append(f"--keep-daily={daily}")
        if weekly:
            args_before.append(f"--keep-weekly={weekly}")
        if monthly:
            args_before.append(f"--keep-monthly={monthly}")
        if yearly:
            args_before.append(f"--keep-yearly={yearly}")
        if within:
            args_before.append(f"--keep-within={within}")

        args_before.append(f"--glob-archives={archive_prefix}*")

        return self.run(args_before=args_before)

    def run(  # noqa: CFQ001 pylint: disable=too-many-arguments,too-many-locals,too-many-branches,too-many-statements,too-complex
        self,
        archive_name: Optional[str] = None,
        args_before: Optional[List[str]] = None,
        args_after: Optional[List[str]] = None,
        pipe_process: Optional[subprocess.Popen[str]] = None,
        env: Optional[Dict[str, str]] = None,
    ) -> BackstepStats:
        """Run Borg."""

        args = [self.borg_path, "--verbose"]

        if self.ssh_key:
            args.extend(("--rsh", f'ssh -i "{self.ssh_key}" -o ConnectTimeout=10'))

        if args_before:
            args.extend(args_before)

        # Add archive URI
        args.append(self._archive_uri(archive_name))

        if args_after:
            args.extend(args_after)

        # Check if we need to port knock
        if self.server_portknock:
            self._portknock()

        # Work out the popen args...
        popen_args: Dict[str, Any] = {
            "args": args,
            "stdout": subprocess.PIPE,
            "stderr": subprocess.STDOUT,
            "env": {},
        }

        # If we have a passphrase file, we need to add the content of it to the env
        if self.passphrase_file:
            try:
                with open(self.passphrase_file, "r", encoding="UTF-8") as file_passphrase:
                    popen_args["env"]["BORG_PASSPHRASE"] = file_passphrase.read()
            except OSError as err:
                raise BackupError(f"Error opening passphrase file: {err}") from None
            self.notifiers.info(f'Using passphrase from "{self.passphrase_file}"')
        else:
            popen_args["env"]["BORG_PASSPHRASE"] = ""  # nosec

        # Check if we have any environment variables passed
        if env:
            popen_args["env"].update(env)

        # Check if we need to setup a pipe
        if pipe_process:
            popen_args["stdin"] = pipe_process.stdout

        # This will store the backup stats and the stats state for this tbackup
        stats: BackstepStats = {}
        stats_state: BackstepStats = {}

        self.notifiers.info("Borg started")
        self.notifiers.debug(f"Executing: {args}")
        try:  # nosec # pylint: disable=too-many-try-statements
            # Create process
            with subprocess.Popen(**popen_args) as process:  # nosec: B603
                # For a borg backup, the summary seems to be json split over lines, so we need to keep track once we have the stats
                # the parse that chunk of data
                summary_output = ""
                # Check if the pipe_process is ok if we have one...
                if pipe_process:
                    result_code_pp = pipe_process.poll()
                    # Check for error condition...
                    if result_code_pp:
                        raise BackupError(f"Error piping data from child process to Borg, exit code {result_code_pp}") from None
                # Loop with output
                for line in chunks(process.stdout, delim="\n\r"):
                    # End of the actual backup process is a { , which is the beginning of the archive info
                    if line == "{":
                        summary_output += line
                        continue
                    # Parse output if we've not reached the end of our output yet
                    if not summary_output:
                        self._parse_output(line, stats, stats_state)
                    # Once we have stats, save the rest in our summary...
                    else:
                        summary_output += line
                # Grab return code
                process.communicate()
                result_code = process.poll()
                # Check the pipe process out
                result_code_pp = 0
                if pipe_process:
                    pipe_process.communicate()
                    result_code_pp = pipe_process.poll()
                # Lastly check if we have a summary_output, if so, try parse it
                if summary_output:
                    self._parse_output_summary(summary_output, stats)
                # Save result code
                if pipe_process:
                    self.notifiers.info(f"Borg completed: status={result_code}, pipe status={result_code_pp}")
                elif not result_code:
                    self.notifiers.info(f"Borg completed: status={result_code} (NORMAL)")
                elif result_code == 1:
                    self.notifiers.info(f"Borg completed: status={result_code} (WARNING)")
                    # Silently ignore
                    result_code = 0
                else:
                    self.notifiers.info(f"Borg completed: status={result_code} (ERROR)")

                stats["result_code"] = result_code or result_code_pp

        except KeyboardInterrupt:
            raise BackupError("Keyboard interrupt while running Borg") from None

        except subprocess.SubprocessError as err:
            raise BackupError(f"Error running Borg: {err}") from None

        except OSError as err:
            raise BackupError(f"Error running Borg: {err}") from None

        return stats

    def _portknock(self) -> None:
        """Port knock the server."""
        self.notifiers.info(f"Port knocking {self.server} on port {self.server_portknock}")
        try:
            addrinfo = socket.getaddrinfo(self.server, self.server_portknock, proto=socket.IPPROTO_TCP)
        except socket.gaierror as err:
            raise BackupError(f'Failed resolving "{self.server}": {err}') from None

        for entry in addrinfo:
            self.notifiers.debug(f"Port knocking {entry[4][0]}")
            sock = socket.socket(entry[0], entry[1])
            sock.settimeout(1)
            with contextlib.suppress(socket.timeout):
                sock.connect(entry[4])
        self.notifiers.info("Port knocking done")

    def _archive_uri(self, archive_name: Optional[str] = None) -> str:
        """Return the Borg archive URI."""

        uri = self.server

        if self.server_user:
            uri = f"{self.server_user}@" + uri

        if self.server_port:
            uri = uri + f":{self.server_port}"

        # Add repo name  /./repositories/xxx  for instance
        uri = uri + self.repo

        if archive_name:
            uri = uri + f"::{archive_name}"

        # This is an SSH URI
        uri = "ssh://" + uri

        return uri

    def _parse_output_summary(self, chunk: str, stats: BackstepStats) -> None:
        """Parse output chunk from borg, which should be our summary."""

        # Make sure the chunk is json
        if chunk.startswith("{") and chunk.endswith("}"):
            # Try decode json
            try:
                chunk_json = json.loads(chunk)
            except json.JSONDecodeError as err:
                self.notifiers.warning(f"Failed to decode JSON output summary: {err}\n{chunk}")
                return
            # Store stats
            stats["duration"] = int(chunk_json["archive"]["duration"])
            stats["backup_size"] = chunk_json["archive"]["stats"]["original_size"]
            stats["backup_size_compressed"] = chunk_json["archive"]["stats"]["compressed_size"]
            stats["backup_size_deduplicated"] = chunk_json["archive"]["stats"]["deduplicated_size"]
            stats["backup_size_actual"] = stats["backup_size_deduplicated"]
            stats["backup_ratio"] = f'{(stats["backup_size_actual"] / stats["backup_size"]) * 100:.2f}'
            stats["total_size"] = chunk_json["cache"]["stats"]["total_size"]
            stats["total_size_compressed"] = chunk_json["cache"]["stats"]["total_csize"]
            stats["total_size_deduplicated"] = chunk_json["cache"]["stats"]["unique_csize"]
            stats["total_size_actual"] = stats["total_size_deduplicated"]
            if not stats["total_size"]:
                stats["total_ratio"] = "1.00"
            else:
                stats["total_ratio"] = f'{(stats["total_size_actual"] / stats["total_size"]) * 100:.2f}'

        else:
            self.notifiers.warning(f"Chunk not understood from borg: {chunk}")

    def _parse_output(  # pylint: disable=too-many-branches,too-many-statements
        self, line: str, stats: BackstepStats, stats_state: Dict[str, Any]
    ) -> None:
        """Parse output line from borg."""

        # For lines starting with { and ending in }, its probably JSON
        if not (line.startswith("{") and line.endswith("}")):
            # Fallthrough
            self.notifiers.info(line)
            return

        # Try decode json, possibly as a stream
        try:
            json_stream = json_loads_stream(line)
        except json.JSONDecodeError as err:
            self.notifiers.warning(f"Failed to decode JSON output: {err}\n{line}")
            return

        while True:  # pylint: disable=while-used
            # Grab next JSON object
            json_obj = next(json_stream, None)
            if json_obj is None:
                break
            # Make sure we have a type attribute
            if "type" not in json_obj:
                self.notifiers.warning(f'JSON line not understood, no "type": {line}')
                return
            # Parse output
            self._parse_output_json(json_obj, stats, stats_state)

    def _parse_output_json(  # pylint: disable=too-many-branches,too-many-statements,too-complex
        self, json_obj: Dict[str, Any], stats: BackstepStats, stats_state: Dict[str, Any]
    ) -> None:
        # Check for generic progress messages
        if (json_obj["type"] == "progress_message") or (json_obj["type"] == "progress_percent"):
            # Line key for the last line we ignored
            msgid_line = f'last_line_{json_obj["msgid"]}'

            if not json_obj["finished"]:
                msgid_state = f'last_progress_{json_obj["msgid"]}'
                # Rate limit the progress_message we output... only if its % based
                if (msgid_state not in stats_state) or ("%" not in json_obj["message"]):
                    stats_state[msgid_state] = 0
                # Make sure at least 10s has elapsed before outputting
                now = timer()
                message = f'- {json_obj["msgid"]}: {json_obj["message"]}'
                if now - stats_state[msgid_state] > 10:
                    self.notifiers.info(message)
                    # Save our last progress as being now
                    stats_state[msgid_state] = now
                else:
                    stats_state[msgid_line] = message
            else:
                # If we didn't display the last message, display it now
                if msgid_line in stats_state:
                    self.notifiers.info(stats_state[msgid_line])
                # Then signal we're done
                self.notifiers.info(f'- {json_obj["msgid"]}: done')

        # Check for generic log messages
        elif json_obj["type"] == "log_message":  # pylint: disable=confusing-consecutive-elif
            if json_obj["name"] in {"borg.archiver", "borg.cache"}:
                self.notifiers.debug(json_obj["message"])

        # Check for archive_progress
        elif json_obj["type"] == "archive_progress":  # pylint: disable=confusing-consecutive-elif
            if "finished" not in json_obj or not json_obj["finished"]:
                # Work out the human readable sizes
                original_size = human_bytes(json_obj["original_size"])
                compressed_size = human_bytes(json_obj["compressed_size"])
                deduplicated_size = human_bytes(json_obj["deduplicated_size"])
                # Make sure we don't divide by zero when processing multiple files
                if json_obj["original_size"]:
                    storage_ratio = (json_obj["deduplicated_size"] / json_obj["original_size"]) * 100
                else:
                    storage_ratio = 0

                # This is a status update on a path being backed up
                if json_obj["path"]:
                    # Rate limit the progress we output...
                    if "last_progress" not in stats_state:
                        stats_state["last_progress"] = 0
                    # Make sure at least 10s has elapsed before outputting
                    now = timer()
                    if now - stats_state["last_progress"] > 10:
                        # Log the update
                        self.notifiers.info(
                            "- Progress => "
                            f'{json_obj["nfiles"]+1} / '
                            f"{original_size:11s} / "
                            f"{compressed_size:11s} / "
                            f"{deduplicated_size:11s} / "
                            f"{storage_ratio:.2f}%"
                        )
                        # Save our last progress as being now
                        stats_state["last_progress"] = now
                else:
                    # Log the update
                    self.notifiers.info(
                        "- Summary: "
                        f'files {json_obj["nfiles"]}, '
                        f"original_size {original_size}, "
                        f"compressed_size {compressed_size}, "
                        f"deduplicated_size {deduplicated_size}, "
                        f"storage_ratio {storage_ratio:.2f}%"
                    )
                    # Record this archives stats
                    stats["backup_files"] = json_obj["nfiles"]
                    stats["backup_size"] = json_obj["original_size"]
                    stats["backup_size_compressed"] = json_obj["compressed_size"]
                    stats["backup_size_deduplicated"] = json_obj["deduplicated_size"]
                    stats["backup_size_actual"] = stats["backup_size_deduplicated"]
                    stats["backup_storage_ratio"] = f"{storage_ratio:.2f}"
            else:
                # We're done
                self.notifiers.info("- Archive progress: done")
        # Fallthrough if we don't understand the message
        else:
            self.notifiers.warning(f'JSON line not understood, unknown "type": {json.dumps(json_obj)}')

    @property
    def notifiers(self) -> NotifierList:
        """Return our notifiers."""
        return self._notifiers

    @property
    def borg_path(self) -> str:
        """Path to 'borg' to execute."""
        return self._borg_path

    @property
    def compression(self) -> str:
        """Return the compression algorithm used."""
        return self._compression

    @property
    def ssh_key(self) -> str:
        """Return the Borg server ssh_key."""
        return self._ssh_key

    @property
    def server(self) -> str:
        """Return the Borg server."""
        return self._server

    @property
    def server_portknock(self) -> Optional[str]:
        """Return the Borg server port-knock port."""
        return self._server_portknock

    @property
    def server_port(self) -> Optional[str]:
        """Return the Borg server_port."""
        return self._server_port

    @property
    def server_user(self) -> Optional[str]:
        """Return the Borg server username."""
        return self._server_user

    @property
    def passphrase_file(self) -> Optional[str]:
        """Return the Borg passphrase file."""
        return self._passphrase_file

    @property
    def repo(self) -> str:
        """Return the Borg server repository."""
        return self._repo


class BorgPlugin(MethodPluginBase):
    """Borg method plugin."""

    _borg: Optional[BorgBackup]
    _notifiers: Optional[NotifierList]

    def __init__(self) -> None:
        """Initialize object."""

        # Call parent __init__
        super().__init__()

        # Initialize our internals
        self._borg = None
        self._notifiers = None

    def register_parser(  # noqa: CFQ001 # pylint: disable=too-many-statements,too-many-branches
        self, subparsers: argparse.Namespace, datasource_parser: argparse.ArgumentParser, notifier_parser: argparse.ArgumentParser
    ) -> List[str]:
        """
        Register command line parsers.

        :param subparsers: argparse subparsers
        :type subparsers: argparse.Namespace
        :param datasource_parser: Datasource parser, used to add as a parent
        :type datasource_parser: argparse.ArgumentParser
        :param notifier_parser: Notifier parser, used to add as a parent
        :type notifier_parser: argparse.ArgumentParser
        :returns: list of commands we support
        :rtype: List[str]
        """

        # We need to return the commands we support
        commands = []

        borg_common_parser = argparse.ArgumentParser(add_help=False)
        borg_common_parser.add_argument("--borg-path", metavar="BORG_PATH", default="borg", help="Path to 'borg', default: 'borg'")
        borg_common_parser.add_argument("--borg-server", required=True, help="Borg server")
        borg_common_parser.add_argument(
            "--borg-server-portknock", type=int, metavar="PORT_KNOCK_PORT", help="Borg server port knocking port"
        )
        borg_common_parser.add_argument(
            "--borg-server-sshkey", required=True, metavar="SSH_KEY_FILE", help="Borg server SSH key path"
        )
        borg_common_parser.add_argument("--borg-server-port", type=int, metavar="SERVER_PORT", help="Borg server SSH port")
        borg_common_parser.add_argument("--borg-server-user", metavar="SERVER_USERNAME", help="Borg server SSH username")
        borg_common_parser.add_argument("--borg-repo", required=True, help="Borg repository path")
        borg_common_parser.add_argument("--borg-passphrase-file", metavar="PASSPHRASE_FILE", help="Borg repository passphrase file")

        # Add borg-backup command
        commands.append("borg-backup")
        backup_parser = subparsers.add_parser(
            "borg-backup", description="Command: borg-backup", parents=[borg_common_parser, datasource_parser, notifier_parser]
        )
        backup_parser.add_argument(
            "--action", action="store_const", const="borg-backup", default="borg-backup", help=argparse.SUPPRESS
        )
        backup_parser.add_argument("--borg-archive-timestamp", help="Borg archive timestamp")
        backup_parser.add_argument("--borg-keep-daily", help="Number of daily backups to keep")
        backup_parser.add_argument("--borg-keep-weekly", help="Number of weekly backups to keep")
        backup_parser.add_argument("--borg-keep-monthly", help="Number of monthly backups to keep")
        backup_parser.add_argument("--borg-keep-yearly", help="Number of yearly backups to keep")
        backup_parser.add_argument("--borg-keep-within", help="Keep backups within this period")
        backup_parser.add_argument("--check-repair", action="store_true", help="Check and repair archive")
        backup_parser.add_argument("--compact", action="store_true", help="Compact after backup")
        backup_parser.add_argument("--pre-backup-script", help="Run this script before we start backing up")

        backup_parser.add_argument("--backup-name", help="Optional backup name (the datasource decides if it will use this)")
        backup_parser.add_argument("backup_item", nargs="+", help="Items to backup, using the respective datasource")

        # Add borg-init command
        commands.append("borg-init")
        init_parser = subparsers.add_parser("borg-init", description="Command: borg-init", parents=[borg_common_parser])
        init_parser.add_argument("--action", action="store_const", const="borg-init", default="borg-init", help=argparse.SUPPRESS)

        # Add borg-check-repair command
        commands.append("borg-check-repair")
        check_repair_parser = subparsers.add_parser(
            "borg-check-repair", description="Command: borg-check-repair", parents=[borg_common_parser]
        )
        check_repair_parser.add_argument(
            "--action", action="store_const", const="borg-check-repair", default="borg-check-repair", help=argparse.SUPPRESS
        )

        # Add borg-compact command
        commands.append("borg-compact")
        compact_parser = subparsers.add_parser("borg-compact", description="Command: borg-compact", parents=[borg_common_parser])
        compact_parser.add_argument(
            "--action", action="store_const", const="borg-compact", default="borg-compact", help=argparse.SUPPRESS
        )

        # Add borg-extract command
        commands.append("borg-extract")
        extract_parser = subparsers.add_parser("borg-extract", description="Command: borg-extract", parents=[borg_common_parser])
        extract_parser.add_argument(
            "--action", action="store_const", const="borg-extract", default="borg-extract", help=argparse.SUPPRESS
        )
        extract_parser.add_argument(
            "--strip-components", nargs=1, type=int, help="Remove the specified number of leading path elements"
        )
        extract_parser.add_argument("--exclude", nargs=1, help="Exclude paths matching pattern")
        extract_parser.add_argument("--pattern", nargs=1, help="Include/exclude paths matching pattern")
        extract_parser.add_argument("path", nargs="*", help="Path(s) to extract")

        # Add borg-list command
        commands.append("borg-list")
        list_parser = subparsers.add_parser("borg-list", description="Command: borg-list", parents=[borg_common_parser])
        list_parser.add_argument("--action", action="store_const", const="borg-list", default="borg-list", help=argparse.SUPPRESS)
        list_parser.add_argument("--glob-archives", nargs=1, help="Only display archives matching glob")
        list_parser.add_argument("--sort-keys", nargs=1, help="Comma-separated list of sorting keys: timestamp,name,id")
        list_parser.add_argument("--first", nargs=1, type=int, help="Display N archives after other filters were applied")
        list_parser.add_argument("--last", nargs=1, type=int, help="Display N archives after other filters were applied")
        list_parser.add_argument("--exclude", nargs=1, help="Exclude paths matching pattern")
        list_parser.add_argument("--pattern", nargs=1, help="Include/exclude paths matching pattern")

        # Add borg-key-import command
        commands.append("borg-key-import")
        keyimport_parser = subparsers.add_parser(
            "borg-key-import", description="Command: borg-key-import", parents=[borg_common_parser]
        )
        keyimport_parser.add_argument(
            "--action", action="store_const", const="borg-key-import", default="borg-key-import", help=argparse.SUPPRESS
        )
        keyimport_parser.add_argument("key_file", nargs=1, help="Key to import")

        # Add borg-delete command
        commands.append("borg-delete")
        delete_parser = subparsers.add_parser("borg-delete", description="Command: borg-delete", parents=[borg_common_parser])
        delete_parser.add_argument(
            "--action", action="store_const", const="borg-delete", default="borg-delete", help=argparse.SUPPRESS
        )
        delete_parser.add_argument("archive_name", nargs="+", help="Archives to delete")

        # Add borg-prune command
        commands.append("borg-prune")
        prune_parser = subparsers.add_parser("borg-prune", description="Command: borg-prune", parents=[borg_common_parser])
        prune_parser.add_argument(
            "--action", action="store_const", const="borg-prune", default="borg-prune", help=argparse.SUPPRESS
        )
        prune_parser.add_argument("--borg-keep-daily", help="Number of daily backups to keep")
        prune_parser.add_argument("--borg-keep-weekly", help="Number of weekly backups to keep")
        prune_parser.add_argument("--borg-keep-monthly", help="Number of monthly backups to keep")
        prune_parser.add_argument("--borg-keep-yearly", help="Number of yearly backups to keep")
        prune_parser.add_argument("--borg-keep-within", help="Keep backups within this period")
        prune_parser.add_argument("archive_prefix", nargs="+", help="Archive prefixes to prune")

        # Return the commands we support
        return commands

    def run(  # pylint: disable=too-complex,too-many-branches
        self,
        command: str,
        notifiers: NotifierList,
        command_args: Dict[str, Any],
        datasource: Optional[DatasourcePluginBase] = None,
    ) -> None:
        """
        Run plugin.

        Parameters
        ----------
        command : :class:`str`
            Command to run.

        notifiers : :class:`NotifierList`
            Notifier list to use.

        command_args : :class:`Dict` [ :class:`str`, :class:`Any` ]
            Command arguments.

        datasource : :class:`Optional` [ :class:`DatasourcePluginBase` ]
            Optional datasource plugin to use.

        Raises
        ------
        RuntimeError
            Raised if the plugin encounters a runtime error.

        """

        # Save internals
        self._notifiers = notifiers
        # Create a borg object
        self._borg = BorgBackup(
            notifiers=self.notifiers,
            borg_path=command_args["borg_path"],
            server=command_args["borg_server"],
            server_portknock=command_args["borg_server_portknock"],
            server_port=command_args["borg_server_port"],
            server_user=command_args["borg_server_user"],
            ssh_key=command_args["borg_server_sshkey"],
            passphrase_file=command_args["borg_passphrase_file"],
            repo=command_args["borg_repo"],
        )
        # If we have a datasource setup its notifiers
        if datasource:
            datasource.notifiers = self.notifiers

        # Check which command was called
        if command == "borg-backup":
            if not datasource:
                raise RuntimeError("No datasource?")
            # See first if we're doing a check-repair combo
            if ("check_repair" in command_args) and command_args["check_repair"]:
                self._borg_check_repair()
            # If not just run the backup
            else:
                self._borg_backup(datasource, command_args)

        elif command == "borg-init":
            self._borg_init()

        elif command == "borg-check-repair":
            self._borg_check_repair()

        elif command == "borg-compact":
            self._borg_compact()

        elif command == "borg-extract":
            self._borg_extract(command_args)

        elif command == "borg-list":
            self._borg_list(command_args)

        elif command == "borg-key-import":
            self._borg_key_import(command_args)

        elif command == "borg-delete":
            self._borg_delete(command_args)

        elif command == "borg-prune":
            self._borg_prune(command_args)

    def _borg_backup(  # pylint: disable=too-many-statements,too-complex
        self, datasource: DatasourcePluginBase, command_args: Dict[str, Any]
    ) -> None:
        if ("pre_backup_script" in command_args) and command_args["pre_backup_script"]:
            self.notifiers.notice(f'>>> RUNNING PRE-BACKUP SCRIPT: {command_args["pre_backup_script"]}')
            if not self._run_script(command_args["pre_backup_script"]):
                self.notifiers.notice(">>> FAILED PRE-BACKUP SCRIPT")
                self.notifiers.send_stats("FAILED PRE-BACKUP SCRIPT", {"result_code": -1})
                return
            self.notifiers.notice(">>> FINISHED RUNNING PRE-BACKUP SCRIPT")

        # Grab the backup set from the datasource
        backup_set = datasource.get_backup_set(
            backup_items=command_args["backup_item"],
            backup_name=command_args["backup_name"],
        )

        # Loop with each archive in the set
        for backup_archive in backup_set.archives:
            # Grab local time
            timestamp_now = datetime.now()

            # Work out the easier to read timestamp
            timestamp = timestamp_now.strftime("%Y%m%d%H%M%S")
            # Base the unix timestamp off UTC
            timestamp_unix = int(time.mktime(datetime.utcnow().timetuple()))

            self.notifiers.notice(f">>> STARTING BACKUP FOR {backup_archive.name}")
            # Setup the topic of this backup
            timestamp_log = timestamp_now.strftime("%Y-%m-%d %H:%M:%S")
            topic = f"backup for {backup_archive.name} @{timestamp_log}"

            # Clear stats so we can test below
            stats: BackstepStats = {
                "result_code": -1,
                # Save additional backup details
                "name": backup_archive.name,
                "datasource": datasource.name,
                "timestamp": timestamp,
                "timestamp_unix": timestamp_unix,
            }

            # Run pre-backup method
            self.notifiers.info("Backup tasks pre-backup started")
            try:
                backup_archive.pre_backup()
                self.notifiers.info("Backup tasks pre-backup completed")
            except DatasourceError as err:
                self.notifiers.error(f"Backup tasks pre-backup failed: {err}")
                self.notifiers.error(f">>> FAILED BACKUP FOR {backup_archive.name}")
                # Send the failed stats to the notifiers
                self.notifiers.send_stats(f"FAILED {topic}", stats)
                return

            # Try the backup
            self.notifiers.info("Starting backup")
            try:
                # Do the actual backup
                backup_stats = self.borg.backup(f"{backup_archive.name}-{timestamp}", backup_archive)
                stats.update(backup_stats)
                self.notifiers.info("Completed backup")
            except BackupError as err:
                self.notifiers.error(f"Failed backup: {err}")

            # Run post-backup method
            self.notifiers.info("Backup tasks post-backup started")
            try:
                backup_archive.post_backup()
                self.notifiers.info("Backup tasks post-backup completed")
            except DatasourceError as err:
                self.notifiers.error(f"Backup tasks post-backup failed: {err}")
                self.notifiers.error(f">>> FAILED BACKUP FOR {backup_archive.name}")
                # Send the failed stats to the notifiers
                self.notifiers.send_stats(f"FAILED {topic}", stats)
                # Update result code, so even if the backup completed, we still trigger an error
                stats["result_code"] = -1
                return

            # If we don't have stats then the backup must of failed
            if not stats["result_code"]:
                # Prune the archive
                self.notifiers.info("Backup prune started")
                prune_stats = self._borg_prune(command_args, [f"{backup_archive.name}-"])
                self.notifiers.info("Backup prune completed")
                # Check the result of the prune
                if prune_stats and not prune_stats["result_code"]:
                    self.notifiers.notice(f">>> COMPLETED BACKUP FOR {backup_archive.name}")
                    topic = "COMPLETED " + topic
                else:
                    self.notifiers.warning(f">>> PARTIALLY COMPLETED BACKUP FOR {backup_archive.name}")
                    topic = "PARTIALLY completed " + topic
                    stats["result_code"] = 1
                # Check if we're doing a compact after backup
                if ("compact" in command_args) and command_args["compact"]:
                    self._borg_compact()
            else:
                self.notifiers.error(f">>> FAILED BACKUP FOR {backup_archive.name}")
                topic = "FAILED " + topic

            # Send the stats to the notifiers
            self.notifiers.send_stats(topic, stats)

    def _borg_init(self) -> None:
        """Initialize repository for the Borg backup."""
        self.notifiers.notice(">>> STARTING INIT")
        self.borg.backup_init()
        self.notifiers.notice(">>> COMPLETED INIT")

    def _borg_check_repair(self) -> None:
        """Do a check repair."""
        self.notifiers.notice(">>> STARTING CHECK REPAIR")
        self.borg.check_repair()
        self.notifiers.notice(">>> COMPLETED CHECK REPAIR")

    def _borg_compact(self) -> None:
        """Do a compact on a Borg repostiory."""
        self.notifiers.notice(">>> STARTING COMPACT")
        self.borg.compact()
        self.notifiers.notice(">>> COMPLETED COMPACT")

    def _borg_extract(self, command_args: Dict[str, Any]) -> None:
        """Extract contents from a Borg repository."""
        self.notifiers.notice(">>> STARTING EXTRACT")
        self.borg.backup_extract(command_args)
        self.notifiers.notice(">>> COMPLETED EXTRACT")

    def _borg_list(self, command_args: Dict[str, Any]) -> None:
        """List archives in the Borg repository."""
        self.notifiers.notice(">>> STARTING LIST")
        self.borg.backup_list(command_args)
        self.notifiers.notice(">>> COMPLETED LIST")

    def _borg_key_import(self, command_args: Dict[str, Any]) -> None:
        # Check if we have a key file
        if ("key_file" not in command_args) or not command_args["key_file"]:
            raise RuntimeError('No "key_file" option')
        self.notifiers.notice(">>> STARTING KEY IMPORT")
        # If so import it
        self.borg.key_import(command_args["key_file"][0])
        self.notifiers.notice(">>> COMPLETED KEY IMPORT")

    def _borg_delete(self, command_args: Dict[str, Any]) -> None:
        # Check if we have archives to delete
        if ("archive_name" not in command_args) or not command_args["archive_name"]:
            raise RuntimeError('No "archive_name" options')
        # If so delete all of them
        for archive_name in command_args["archive_name"]:
            self.notifiers.notice(f">>> STARTING DELETE FOR {archive_name}")
            self.borg.backup_delete(archive_name, command_args)
            self.notifiers.notice(f">>> COMPLETED DELETE FOR {archive_name}")

    def _borg_prune(  # pylint: disable=too-complex,too-many-branches
        self, command_args: Dict[str, Any], archive_prefixes: Optional[List[str]] = None
    ) -> BackstepStats:
        # Check if we got an archive prefix as a parameter
        if archive_prefixes:
            _archive_prefixes = archive_prefixes
        # If not, check if its in the command_args
        elif ("archive_prefix" in command_args) and command_args["archive_prefix"]:
            _archive_prefixes = command_args["archive_prefix"]
        # Failing that raise an error
        else:
            raise RuntimeError("No archive prefix options")

        # Work out options we're going to be passing
        prune_args: Dict[str, str] = {}
        if "borg_keep_daily" in command_args:
            prune_args["daily"] = command_args["borg_keep_daily"]
        if "borg_keep_weekly" in command_args:
            prune_args["weekly"] = command_args["borg_keep_weekly"]
        if "borg_keep_monthly" in command_args:
            prune_args["monthly"] = command_args["borg_keep_monthly"]
        if "borg_keep_yearly" in command_args:
            prune_args["yearly"] = command_args["borg_keep_yearly"]
        if "borg_keep_within" in command_args:
            prune_args["within"] = command_args["borg_keep_within"]

        # Lets track the result code and if anything failed
        prune_stats: BackstepStats = {"result_code": 0}

        # If we don't have any prune arguments, just return
        if not prune_args:
            return prune_stats

        # Prune all archive prefixes provided
        for archive_prefix in _archive_prefixes:
            self.notifiers.info(f"Starting prune for: {archive_prefix}")
            stats = self.borg.backup_prune(archive_prefix, **prune_args)
            # Setup prune stats result code
            if "result_code" in stats:
                prune_stats["result_code"] = stats["result_code"]
            # Check the result code we have
            if not prune_stats["result_code"]:
                self.notifiers.info(f"Completed prune for: {archive_prefix}")
            else:
                self.notifiers.error(f"Failed prune for: {archive_prefix}")

        return prune_stats

    def _run_script(self, script: str) -> bool:
        """
        Run a script.

        :param script: Script to run
        :type script: str
        :returns: either True indicating success or False indicating failure
        """

        # Work out the popen args...
        popen_args: Dict[str, Any] = {
            "args": [script],
            "stdout": subprocess.PIPE,
            "stderr": subprocess.STDOUT,
            "env": {},
        }

        self.notifiers.debug(f"Executing: {script}")
        try:  # pylint: disable=too-many-try-statements
            # Create process
            with subprocess.Popen(**popen_args) as process:  # nosec
                # Loop with output
                for line in chunks(process.stdout, delim="\n\r"):
                    self.notifiers.info(f"pre-backup-script: {line}")
                # Grab return code
                process.communicate()
                result_code = process.poll()
                # If we failed return false
                if result_code:
                    return False

        except KeyboardInterrupt:
            raise BackupError("Keyboard interrupt while running script") from None

        except subprocess.SubprocessError as err:
            raise BackupError(f"Error running script: {err}") from None

        except OSError as err:
            raise BackupError(f"Error running script: {err}") from None

        return True

    @property
    def borg(self) -> BorgBackup:
        """Return the borg property."""

        if not self._borg:
            raise RuntimeError("_borg used but not set")

        return self._borg

    @property
    def notifiers(self) -> NotifierList:
        """Return our notifiers."""

        if not self._notifiers:
            raise RuntimeError("_notifiers used but not set")

        return self._notifiers
