
[![Latest Release](https://gitlab.linux.community/awit-backstep/awit-backstep/-/badges/release.svg)](https://gitlab.linux.community/awit-backstep/awit-backstep/-/releases)
[![pipeline status](https://gitlab.linux.community/awit-backstep/awit-backstep/badges/master/pipeline.svg)](https://gitlab.linux.community/awit-backstep/awit-backstep/-/commits/master)


# AllWorldIT Backstep

AWIT-Backstep is a backup system that allows for multiple methods of backup and multiple datasources along with automated backups.


## Datasources


### Libvirt

The `libvirt` data source is used to backup libvirt virtual machines.

IMPORTANT: Only `qcow2` disk images are supported at present!

It does this by doing an FSTRIM on the qcow2 file, then snapshotting it.

The basic syntax would be...
```sh
backstep ... --datasource libvirt VM1 VM2 VM3
```


### LVM

The `lvm` data source is used to snapshot LVM volumes.

The basic syntax would be...
```sh
backstep ... --datasource lvm vg1/lv1,vg1/lv1-2 vg1/lv2 vg2/lvX
```

The snapshot size can also be specified by appending :SIZE like this...
```sh
backstep ... --datasource lvm vg1/lv1 vg1/lv2:20G
```

### Pipe

The `pipe` datasource is used to pipe data from a command into awit-backstep and store it as a file using the method chosen.

The basic syntax would be...
```sh
backstep ... --datasource pipe FILENAME -- CMD ARG1 ARG2 ARG3
```


### MySQL

The `mysql` datasource is used to pipe data from mysqldump into awit-backstep and store it as a file using the method chosen.

The `backup_items` are passed to `mysqldump`.

The basic syntax to use this datasource would be...
```sh
backstep ... --datasource mysql -- --all-databases --single-transaction
```


# Cheatsheet

## Listing backups

One can add Borg arguments to `borg list`...
```sh
backstep borg-list ... -- --last 2
```



